import { processList, photoList, sendResult,fetchAllData, sendUpdateResult } from './service';
import { history } from 'umi';
import { message} from 'antd';
import {createResult, updateResult} from "@/pages/exam/create/service";
import {queryUserAllPhotos, queryUserBigPhoto, queryUserIncidents} from "@/pages/test/studentInfo/service";

const pick = (obj, arr) =>
  arr.reduce((iter, val) => (val in obj && (iter[val] = obj[val]), iter), {});

const initValue = {
  title:'',
  registerStartTime:'',
  registerEndTime:'',
  remindBeforeEnd:0,
  startTime:'',
  endTime:'',
  description:'',
  paper: {id:-1},
  signUpType:0,
  examineeCsvUrl:[],
  invigilateType:0,
  type:0,
  processCfg:{},
  monitorConfig:{},
  subject:"",
  paperLink:""
}

const keyMap = ['id', 'title', 'registerStartTime','registerEndTime','startTime','endTime','description','paper','signUpType','examineeCsvUrl','invigilateType','type','processCfg','monitorConfig','subject','paperLink','remindBeforeEnd'];

const Model = {
  namespace: 'testCreate',
  state: initValue,
  effects: {
    *fetch({ payload }, { call, put, all }) {

      const [response1, response2]  = yield all([
        call(processList, payload),
        call(photoList, payload),
      ])

      if(response1){
        yield put({
          type: 'processList',
          payload: response1.status == 'ok' ? response1.data : {},
        });
      }

      if(response2){
        yield put({
          type: 'photoList',
          payload: response2.status == 'ok' ? response2.data : {},
        });
      }

    },

    *fetchAllData({ payload }, { call, put }) {
      const response = yield call(fetchAllData, payload);
      yield put({
        type: 'init',
        payload: response.status == 'ok' ? response.data :  JSON.parse(JSON.stringify(initValue))
      });
    },

    *submitResult({payload}, { call, put }) {
      let cachePayload = JSON.parse(JSON.stringify(payload));
      if(cachePayload.examineeCsvUrl.length >0){
        cachePayload.examineeCsvUrl = cachePayload.examineeCsvUrl[0].url
      }
      else{
        cachePayload.examineeCsvUrl = "";
      }
      if(cachePayload.paper.id === -1 ){
        cachePayload.paper = null;
      }
      let response;

      if(cachePayload.id){
        response = yield call(sendUpdateResult, cachePayload);
      }
      else{
        response = yield call(sendResult, cachePayload);
      }

        if(response.status === "ok"){
          history.push('/test');
        }
        else{
            message.error(response.message)
        }
    }
  },
  reducers: {
    init(state, action){
      // const {...obj} =  action.payload;
      let obj = pick(action.payload, keyMap);
      obj.examineeCsvUrl = obj.examineeCsvUrl!==""? [{uid:'-1',name: obj.examineeCsvUrl, status:'done', url: obj.examineeCsvUrl}]:[];
      obj.paper = action.payload.paper?obj.paper:{id:-1};
      return obj;
    },
    modifyState(state, action){
      let stateObj = JSON.parse(JSON.stringify(state));
      stateObj[action.payload.key] = action.payload.value;
      return stateObj;
    },
    modifyTimeState(state, action){
      let stateObj = JSON.parse(JSON.stringify(state));
      switch(action.payload.key){
        case 'register':
          stateObj.registerStartTime= action.payload.value[0];
          stateObj.registerEndTime= action.payload.value[1];
          break;
        case 'test':
          stateObj.startTime= action.payload.value[0];
          stateObj.endTime= action.payload.value[1];
          break;
      }

      return stateObj;
    },
    processList(state, action){
      let stateObj = JSON.parse(JSON.stringify(state));
      stateObj['processCfg'] = action.payload;
      return stateObj;
    },
    photoList(state, action){
      let stateObj = JSON.parse(JSON.stringify(state));
      stateObj['monitorConfig'] = action.payload;
      return stateObj;
    },
    reset(state, action){
      return   JSON.parse(JSON.stringify(initValue)) ;
    }
  },
};
export default Model;
