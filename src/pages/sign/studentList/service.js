import request from 'umi-request';


export async function queryFakeList(params) {
  const paramsStr_= (params.searchAccount?`&account=${params.searchAccount}`:'') + (params.searchName?`&name=${params.searchName}`:'');
  return request( API_SERVER + `backend-service/api/sign-up/user/list?page=${params.pagination.current}&size=${params.pagination.pageSize}&signUpId=${params.id}`+paramsStr_, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}

export async function getInfo(params) {
  return request( API_SERVER + `backend-service/api/sign-up/info?signUpId=${params.id}`, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}
