import { PlusOutlined } from '@ant-design/icons';
import React, { Component  } from 'react';
import {Row, Col, Divider, Checkbox} from 'antd';
import { Tabs , Input, Card ,Button , Modal,Select,Spin, Popover,DatePicker,Upload,message } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
import moment from 'moment';
import 'moment/locale/zh-cn';
import locale from 'antd/lib/locale/zh_CN';
import { UploadOutlined } from '@ant-design/icons';
import debounce from 'lodash/debounce';
import Authorized from "@/utils/Authorized";


const { RangePicker } = DatePicker;

class testEdit extends Component {
  constructor() {
    super();
    this.state= {
      paperList:[],
      fetching: false,
    }
    this.submit = this.submit.bind(this);
    this.fetchPapers = debounce(this.fetchPapers, 800);
    this.lastFetchId = 0;
  }
  componentDidMount() {
    const { dispatch } = this.props;
    const params =this.props.match.params.id;
    dispatch({
      type: 'testEdit/fetch',
      payload: {
        id:params
      }
    });
   this.fetchPapers('')
  }

  submit(){
    const { dispatch } = this.props;
    dispatch({
      type: 'testEdit/submitResult',
      payload:this.props.testEdit
    });

  }

  fetchPapers(params){
    this.lastFetchId += 1;
    const fetchId = this.lastFetchId;
    this.setState({ data: [], fetching: true });
    fetch(API_SERVER + 'backend-service/api/paper/list?page=1&size=100&keyword='+params,
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
      .then(response => response.json())
      .then(body => {
        if (fetchId !== this.lastFetchId) {
          // for fetch callback order
          return;
        }

        if(body.status =='ok'){
          this.setState({paperList:body.data.list});
        }
        this.setState({ fetching: false });
      });
  }

  changeState(key,e){
    this.props.dispatch({
      type: 'testEdit/modifyState',
      payload: {
        key:key,
        value: e.target.value,
      },
    });
  }

  changeStateValue(key,value){
    this.props.dispatch({
      type: 'testEdit/modifyState',
      payload: {
        key:key,
        value: value,
      },
    });
  }

  changePaperValue(value){
    let changedValue ={id:value.value, title:value.label};
    this.props.dispatch({
      type: 'testEdit/modifyState',
      payload: {
        key:'paper',
        value: changedValue,
      },
    });
  }


  onRegisterOk(key, value) {
    value=[value[0]?moment(value[0]).valueOf():'',value[1]?moment(value[1]).valueOf():''];
    this.props.dispatch({
      type: 'testEdit/modifyTimeState',
      payload: {
        key:key,
        value: value
      },
    });
  }

  updateProcess(index, e) {
    let temp = JSON.parse(JSON.stringify( this.props.testEdit.processCfg));
    temp.tasks[index].checked = e.target.checked;
    this.props.dispatch({
      type: 'testEdit/modifyState',
      payload: {
        key:'processCfg',
        value: temp,
      },
    });
  }

  render() {

    const {
      testEdit: {},
      loading,
    } = this.props;


    const { TextArea } = Input;
    const { Option } = Select;
    const self =this;

    const props = {
      action: API_SERVER + 'upload-service/api/upload/file',
      headers: {
        'x-auth-token': localStorage.getItem("token"),
      },
      onChange(info) {
        let fileList = [...info.fileList];

        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-1);

        // 2. Read from response and show file link
        fileList = fileList.map(file => {
          if (file.response && file.response.status == 'ok') {
            // Component will show file.url as link
            file.url = file.response.data.url;
          }
          return file;
        });

        self.changeStateValue('examineeCsvUrl',fileList)
      },
    };



    const content = (
      <div className={styles.pageHeaderContent}>
        <span className={styles.title}>{this.props.testEdit.title}</span>
           <Button type="primary" className={styles.buttonGroups} onClick={this.submit}>提交</Button>
      </div>
    );

    return (
      <Spin spinning={loading?loading:false}>
        <PageContainer content={content} >
          <Row className={styles.testContainer}  >
            <Divider orientation="left">{examSetting['/test/create']['divider1']}</Divider>
            <Col span={12} className={styles.formArea}>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-1']}</Col>
                <Col span={16} ><Input  className={styles.editGroup} onChange={this.changeState.bind(this,'title')} value={this.props.testEdit.title}/></Col>
              </Row>
              <Row gutter={[8,16]}>  <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-8']}</Col>
                <Col span={16} ><Input  className={styles.editGroup} onChange={this.changeState.bind(this,'subject')} value={this.props.testEdit.subject}/></Col>
              </Row>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-2']}</Col>
                <Col span={16} ><RangePicker
                  showTime={{ format: 'HH:mm:ss' }}
                  format="YYYY-MM-DD HH:mm:ss"
                  style={{width:'100%'}}
                  onOk={this.onRegisterOk.bind(this,'register')}
                  value={[this.props.testEdit.registerStartTime == ''?null: moment(this.props.testEdit.registerStartTime),
                    this.props.testEdit.registerEndTime == ''?null: moment(this.props.testEdit.registerEndTime)]}
                /></Col>
              </Row>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-3']}</Col>
                <Col span={16} ><RangePicker
                  showTime={{ format: 'HH:mm:ss' }}
                  format="YYYY-MM-DD HH:mm:ss"
                  style={{width:'100%'}}
                  onOk={this.onRegisterOk.bind(this,'test')}
                  value={[this.props.testEdit.startTime == ''?null: moment(this.props.testEdit.startTime),
                    this.props.testEdit.endTime == ''?null: moment(this.props.testEdit.endTime)]}
                /></Col>
              </Row>
              <Row gutter={[8,16]}>  <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-4']}</Col>
                <Col span={16} ><Input  className={styles.editGroup} onChange={this.changeState.bind(this,'description')} value={this.props.testEdit.description}/></Col>
              </Row>
            </Col>
            <Col span={12} className={styles.formArea}>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-5']}</Col>
                <Col span={16} ><Select  style={{ width: '100%',textAlign:"left"}} showSearch={true}
                                         notFoundContent={this.state.fetching ? <Spin size="small" /> : <div>Not Found</div>}
                                         filterOption={false}
                                         onSearch={this.fetchPapers.bind(this)}
                                         onChange={this.changePaperValue.bind(this)}
                                         value={{value:this.props.testEdit.paper.id === -1?"":this.props.testEdit.paper.id}}
                                         labelInValue
                                         >
                  {this.state.paperList.map(d => (
                    <Option key={d.id} value={d.id}>{d.title}</Option>
                  ))}
                </Select></Col>
              </Row>
              <Row gutter={[8,16]}>  <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-9']}</Col>
                <Col span={16} ><Input  className={styles.editGroup} onChange={this.changeState.bind(this,'paperLink')} value={this.props.testEdit.paperLink}/></Col>
              </Row>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-6']}</Col>
                <Select  style={{ width: 120,textAlign:"left",margin:'0px 4px'}} onChange={this.changeStateValue.bind(this,'signUpType')} value={this.props.testEdit.signUpType}>
                  <Select.Option value={0}>线下报名</Select.Option>
                </Select>
              </Row>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-7']}</Col>
                <Col span={16} style={{ textAlign:"left"}}>
                  <Upload   {...props} fileList={this.props.testEdit.examineeCsvUrl}>
                    <Button icon={<UploadOutlined />}>Click to Upload</Button>
                  </Upload>
                </Col>
              </Row>
            </Col>
            <Divider orientation="left">{examSetting['/test/create']['divider2']}</Divider>
            <Col span={12} className={styles.formArea}>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area2-1']}</Col>
                <Select  style={{ width: 120,textAlign:"left",margin:'0px 4px'}} onChange={this.changeStateValue.bind(this,'invigilateType')} value={this.props.testEdit.invigilateType}>
                  <Select.Option value={0}>不监考</Select.Option>
                  <Select.Option value={1}>拍照</Select.Option>
                  <Select.Option value={2}>直播</Select.Option>
                </Select>
              </Row>
            </Col>

            <Divider orientation="left">{examSetting['/test/create']['divider3']}</Divider>
            <Col span={12} className={styles.formArea} style={{'textAlign':'left','marginLeft':'36px'}}>
              {
                this.props.testEdit.processCfg.tasks && this.props.testEdit.processCfg.tasks.map((item,index) => {
                  return item.mandatory? <Checkbox key={index} checked={item.checked} disabled={true}> {item.taskName}</Checkbox> :  <Checkbox key={index} onChange={this.updateProcess.bind(this,index)} checked={item.checked}> {item.taskName}</Checkbox>
                })
              }
            </Col>
          </Row>
        </PageContainer>
      </Spin>
    );
  }
}

export default connect(({ testEdit, loading }) => ({
  testEdit,
  loading: loading.models.testEdit,
}))(testEdit);
