import request from 'umi-request';

export async function queryFakeList(params) {
  const query = '&sort=id,desc';
  // const query = '';
  return request(API_SERVER + `backend-service/api/paper/list?page=${params.pagination.current}&size=${params.pagination.pageSize}`+query+`&keyword=${params.searchWords}`, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}

export async function deleteExam(params) {
  // const query = '';
  return request(API_SERVER + `backend-service/api/paper/delete?id=${params.id}`, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}
