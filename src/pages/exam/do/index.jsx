import { PlusOutlined } from '@ant-design/icons';
import React, { Component  } from 'react';
import {Row, Col, Divider, Button, Spin, Modal, Input, Select} from 'antd';
import { Radio} from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { connect  } from 'umi';
import styles from './style.less';


class DoExam extends Component {
  constructor() {
    super();
    this.state={
      isModalVisible: false,
    }
  }
  componentDidMount() {
    const { dispatch } = this.props;
    const params =this.props.match.params.id;
    dispatch({
      type: 'doExam/fetch',
      payload: {
        id: params,
      },
    });
  }

  submit(){
    this.showModal();
  }

  onChange(index,e){

    let currentSubject = this.props.doExam.quizes[index];
    for(let i=0;i<currentSubject.options.length;i++){
      currentSubject.options[i].isRight = false;
    }
    currentSubject.options[e.target.value].isRight = true;
    this.props.dispatch({
      type: 'doExam/modifyQuiz',
      index:index,
      value:currentSubject
    })
  }

  getAnswerIndex(options){
    for(let i=0;i<options.length;i++){
      if(options[i].isRight){
        return i;
      }
    }
    return -1;
  }

  showModal() {
      this.setState({
        isModalVisible:true
      })
  }

  handleOk() {
    this.setState({
      isModalVisible:false
    },function(){
      const { dispatch } = this.props;
      dispatch({
        type: 'doExam/submit',
        payload:this.props.doExam
      });
    })
  }

  handleCancel(){
    this.setState({isModalVisible:false})
  }

  render() {

    const {
      doExam: {},
      loading,
    } = this.props;


    const content = (
      <div className={styles.pageHeaderContent}>
      <Button type="primary" className={styles.buttonGroups} onClick={this.submit.bind(this)}>提交</Button>
      </div>
    );

    let res = [];
      for(let i = 0; i < this.props.doExam.quizes.length; i++) {
        res.push(
          <div className={styles.quizCard} key={i}>
            <p>{(i+1)+'. '+this.props.doExam.quizes[i].description}</p>
            <Radio.Group onChange={this.onChange.bind(this,i)} className={styles.quizOptions} value={this.getAnswerIndex(this.props.doExam.quizes[i].options)}>
              <Radio value={0}>{this.props.doExam.quizes[i].options[0].description}</Radio>
              <Radio value={1}>{this.props.doExam.quizes[i].options[1].description}</Radio>
              <Radio value={2}>{this.props.doExam.quizes[i].options[2].description}</Radio>
              <Radio value={3}>{this.props.doExam.quizes[i].options[3].description}</Radio>
            </Radio.Group>
          </div>
        )
      }

    const modalInfo = (
      <div>
        <p>提交后将无法更改！</p>
      </div>
    )

    return (
      <Spin spinning={loading}>
        <PageContainer content={content} >
          <Row className={styles.quizContainer}  >
            <Col className={styles.quizInfo} span={16} offset={4}>
              {res}
            </Col>
          </Row>
        </PageContainer>
        <Modal title="提交" visible={this.state.isModalVisible} onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)} closable={false}>
          {modalInfo}
        </Modal>
      </Spin>
    );
  }
}

export default connect(({ doExam, loading }) => ({
  doExam,
  loading: loading.models.doExam,
}))(DoExam);
