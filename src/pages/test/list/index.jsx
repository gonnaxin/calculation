import { PlusOutlined } from '@ant-design/icons';
import {Button, Card, Input, List, Tabs, Typography,} from 'antd';
import React, { Component } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
const { Paragraph } = Typography;
import { history } from 'umi';
import moment from 'moment';

const { TabPane } = Tabs;

function callback(key) {

}


class TestList extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'testList/fetch',
      payload: {
        type: 0,
      },
    });
    dispatch({
      type: 'testList/fetchDoing',
      payload: {
        type: 1,
      },
    });
    dispatch({
      type: 'testList/fetchFinish',
      payload: {
        type: 2,
      },
    });
  }

  jump(id,examStatus){
    history.push(`/test/main/${id}?examStatus=${examStatus}`);
  }
  createNewPage(){
    history.push('/test/create');
  }

  render() {
    const {
      loading,
      loading1,
      loading2
    } = this.props;

    const {
      list,
      doingList,
      finishList,
    } = this.props.testList;

    const content = (
      <div className={styles.pageHeaderContent}>

      </div>
    );

    const cardInfo = (item) =>{
      return  <div>
        <Paragraph
          ellipsis={{
            rows: 1,
          }}
        >
          {'学科： ' + item.subject}
        </Paragraph>
        <Paragraph
          ellipsis={{
            rows: 1,
          }}
        >
          {'考试时间： '+moment(item.startTime).format('YYYY-MM-DD HH:mm:ss') +' - '+moment(item.endTime).format('YYYY-MM-DD HH:mm:ss') }
        </Paragraph>
        <Paragraph
          ellipsis={{
            rows: 1,
          }}
        >
          {'试卷： ' + (item.paper?item.paper.title:"")}
        </Paragraph>
        <Paragraph
          ellipsis={{
            rows: 1,
          }}
        >
          {'最后修改时间： ' + moment(item.updated).format('YYYY-MM-DD HH:mm:ss')}
        </Paragraph>
      </div>
    }

    const nullData = {};
    return (
      <PageContainer content={content} className={styles.containerWrap}>
      <div className={styles.wrapper}>
        <Tabs defaultActiveKey="1" >

          <TabPane tab="未开始" key="1">
            <div className={styles.cardList}>
              <List
                rowKey="id"
                loading={loading}
                grid={{
                  gutter: 48,
                  xs: 1,
                  sm: 1,
                  md: 1,
                  lg: 2,
                  xl: 2,
                  xxl: 2,
                }}
                dataSource={[...list]}
                renderItem={(item) => {
                  if (item && item.id) {
                    return (
                      <List.Item key={item.id}>
                        <Card
                          hoverable
                          className={styles.card}
                          actions={[<a key="option1" onClick={this.jump.bind(this,item.id, 0)}>打开</a>]}
                        >
                          <Card.Meta
                            // avatar={<img alt="" className={styles.cardAvatar} src={item.avatar} />}
                            title={<a>{item.title}</a>}
                            description={
                              cardInfo(item)
                            }
                          />
                        </Card>
                      </List.Item>
                    );
                  }


                }}
              />
            </div>
          </TabPane>
          <TabPane tab="进行中" key="2">
            <div className={styles.cardList}>
              <List
                rowKey="id"
                loading={loading1}
                grid={{
                  gutter: 48,
                  xs: 1,
                  sm: 1,
                  md: 1,
                  lg: 2,
                  xl: 2,
                  xxl: 2,
                }}
                dataSource={[...doingList]}
                renderItem={(item) => {
                  if (item && item.id) {
                    return (
                      <List.Item key={item.id}>
                        <Card
                          hoverable
                          className={styles.card}
                          actions={[<a key="option1" onClick={this.jump.bind(this,item.id, 1)}>打开</a>]}
                        >
                          <Card.Meta
                            // avatar={<img alt="" className={styles.cardAvatar} src={item.avatar} />}
                            title={<a>{item.title}</a>}
                            description={
                              cardInfo(item)
                            }
                          />
                        </Card>
                      </List.Item>
                    );
                  }


                }}
              />
            </div>
          </TabPane>
          <TabPane tab="已结束" key="3">
            <div className={styles.cardList}>
              <List
                rowKey="id"
                loading={loading2}
                grid={{
                  gutter: 48,
                  xs: 1,
                  sm: 1,
                  md: 1,
                  lg: 2,
                  xl: 2,
                  xxl: 2,
                }}
                dataSource={[...finishList]}
                renderItem={(item) => {
                  if (item && item.id) {
                    return (
                      <List.Item key={item.id}>
                        <Card
                          hoverable
                          className={styles.card}
                          actions={[<a key="option1" onClick={this.jump.bind(this,item.id,2)}>打开</a>]}
                        >
                          <Card.Meta
                            // avatar={<img alt="" className={styles.cardAvatar} src={item.avatar} />}
                            title={<a>{item.title}</a>}
                            description={
                             cardInfo(item)
                            }
                          />
                        </Card>
                      </List.Item>
                    );
                  }


                }}
              />
            </div>
          </TabPane>
        </Tabs>

          <Button type="primary" className={styles.createButton}  onClick={this.createNewPage.bind(this)}>添加</Button>
      </div>


      </PageContainer>
    );
  }
}

export default connect(({ testList, loading }) => ({
  testList,
  loading: loading.effects['testList/fetch'],
  loading1: loading.effects['testList/fetchDoing'],
  loading2: loading.effects['testList/fetchFinish'],
}))(TestList);
