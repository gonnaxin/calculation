import { queryFakeList } from './service';

const Model = {
  namespace: 'listAndcardList',
  state: {
    list: [],
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      if(response){
        yield put({
          type: 'queryList',
          payload: response.code == 200 ? response.data.list : [],
        });
      }
    },
  },
  reducers: {
    queryList(state, action) {
      return { ...state, list: action.payload };
    },
  },
};
export default Model;
