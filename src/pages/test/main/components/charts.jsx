import React, {useEffect, useMemo, useRef, useState} from "react";
import {connect} from "umi";
import {Space, Row, Col, Spin} from "antd";
import styles from "./charts.less"

import { 	 G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util} from 'bizcharts';

const ChartsPage = (props) => {
  const {examId}= props;

  const [loading, setLoading]= useState(false);
  const [chartsData, setChartsData]= useState({});
  useEffect(() => {
    fetchCharts(examId)
  }, []);


  const fetchCharts = (examId) => {
    setLoading(true);
    fetch(API_SERVER + `backend-service/api/exam/statistic?id=`+examId,
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
      .then(response => response.json())
      .then(body => {

        if(body.status =='ok'){
          setChartsData(body.data);
        }
        setLoading(false)
      });
  }


  const data = [
    {
      type: "总人数",
      number: chartsData.total
    },
    {
      type: "注册人数",
      number: chartsData.registered
    },
    {
      type: "考试人数",
      number: chartsData.examed
    }
  ];
  const cols = {
    // number: {
    //   tickInterval: 10
    // }
  };

  return (
    props.examStatus > 0 ?
      <Spin spinning={loading}>
        <div style={{'display':'inline-block'}}>
          <div className={styles.card} >
            <Chart height={350} data={data} scale={cols} forceFit >
              <Axis name="type" />
              <Axis name="number" />
              <Tooltip
                // crosshairs用于设置 tooltip 的辅助线或者辅助框
                // crosshairs={{
                //  type: "y"
                // }}
              />
              <Geom type="interval" position="type*number" />
            </Chart>
          </div>


        </div>
      </Spin>
    :<div>考试未开始，没有数据</div>
  );
};

export default ChartsPage;
