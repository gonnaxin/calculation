import { queryFakeList } from './service';

const Model = {
  namespace: 'StudentVideo',
  state: {
    info: {},
    pagination: {
      current: 1,
      pageSize: 1,
      total:0
    }
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      if(response){
        yield put({
          type: 'getData',
          payload:  response,
        });
      }
    },

  },
  reducers: {
    getData(state, action) {
      return { ...state, info: action.payload.token };
    },
  },
};
export default Model;
