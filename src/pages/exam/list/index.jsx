import { PlusOutlined } from '@ant-design/icons';
import { Button, Table, Typography ,Space, Input} from 'antd';
import React, { Component } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
const { Paragraph } = Typography;
import { history } from 'umi';
import { SearchOutlined } from '@ant-design/icons';



class examList extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'examList/fetch',
      payload: {
        pagination:this.props.pagination,
        searchWords:this.props.searchWords
      },
    });
  }

  jump(id){
    history.push(`/do/${id}`);
  }

  handleTableChange = (pagination, filters, sorter,e) => {
    // console.log(pagination,filters, sorter)
    // this.fetch({
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   pagination,
    //   ...filters,
    // });

    //much action may activate table change,only handle paginate part
    if(e.action != 'paginate'){
      return;
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'examList/fetch',
      payload: {
        pagination:{
          current: pagination.current,
          pageSize:  pagination.pageSize,
          total: pagination.total,
        },
        searchWords:this.props.searchWords
      },
    });
  };

  createNewPage(){
    history.push('/exam/create');
  }
  edit = (id)=>{
    history.push('/exam/edit/'+id);
  }
  delete = (id)=>{
    const { dispatch } = this.props;
    let obj = {
      current: this.props.pagination.current,
      pageSize: this.props.pagination.pageSize,
      total: this.props.pagination.total,
    }
    dispatch({
      type: 'examList/delete',
      payload: {
        id: id,
        pagination:obj,
        searchWords:this.props.searchWords
      },
    });
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
  })

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    const { dispatch } = this.props;
    dispatch({
      type: 'examList/fetch',
      payload: {
        pagination:this.props.pagination,
        searchWords:selectedKeys[0]?selectedKeys[0]:''
      },
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    const { dispatch } = this.props;
    dispatch({
      type: 'examList/fetch',
      payload: {
        pagination:this.props.pagination,
        searchWords:'',
      },
    });
  };

  render() {
    const columns = [
      {
        key:1,
        title: '试卷名称',
        dataIndex: 'title',
        // sorter: true,
        // sortOrder:'descend',
        width: '20%',
        ...this.getColumnSearchProps('title'),
      },
      {
        key:2,
        title: '应用次数',
        dataIndex: 'referCount',
        width: '15%',
      },
      {
        key:3,
        title: '题目数量',
        dataIndex: 'quizNum',
        width: '15%',
      },
      {
        key:4,
        title: '考试时间',
        dataIndex: 'duration',
        width: '15%',
      },
      {
        key:5,
        title: '总分',
        dataIndex: 'score',
        width: '15%',
      },
      {
        key:6,
        title: '操作',
        dataIndex: 'operations',
        width: '20%',
        render: (text, record) => {
          let delete_ = record.referCount == 0 ? <a onClick={() =>(this.delete(record.id))}>删除</a>:'';
          return (
            <Space size="middle">
              <a onClick={() =>(this.edit(record.id))}>编辑</a>
              {delete_}
            </Space>
          )
        }
      },
    ];

    const {
      examList,
      pagination,
      loading,
    } = this.props;
    const content = (
      <div className={styles.pageHeaderContent}>
        <Button type="primary" className={styles.buttonGroups} onClick={this.createNewPage.bind(this)}>创建试卷</Button>
      </div>
    );
    const nullData = {};
    return (
      <PageContainer content={content} className={styles.containerWrap}>
        <Table
          columns={columns}
          rowKey={record => record.id}
          dataSource={examList}
          pagination={pagination}
          loading={loading}
          onChange={this.handleTableChange}
        />
      </PageContainer>
    );
  }
}

export default connect(({ examList, loading }) => ({
  examList:examList.list,
  pagination:examList.pagination,
  searchWords:examList.searchWords,
  loading: loading.models.examList,
}))(examList);
