import React, { Component  } from 'react';
import {Row, Col, Divider, Checkbox} from 'antd';
import { Tabs , Input, Card ,Button , Modal,Select,Spin, Popover ,Radio, message, Upload} from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
import { UploadOutlined } from '@ant-design/icons';
import add from '@/assets/add.svg';
import deleteIcon from '@/assets/delete.svg';

const { TabPane } = Tabs;

function callback(key) {

}

const optionMap = {
  100:'无',
  0:'A',
  1:'B',
  2:'C',
  3:'D',
  4:'E',
  5:'F',
  6:'G',
  7:'H',
  8:'I',
  9:'J',

}

class ExamPage extends Component {
  constructor() {
    super();
    this.state= {
      isModalVisible: false,
      isAddModalVisible: false,
      popoverVisible: {
        "-1": false,
      },
      parentIndex:-1,
      currentIndex: 0,
      currentSubject: {}
    }
  }
  componentDidMount() {
    const { dispatch } = this.props;
    const {id} = this.props.match.params;

    if(id){
      dispatch({
        type: 'examPage/fetch',
        payload:id
      });
    }
    else{
      dispatch({
        type: 'examPage/reset',
      });
    }

  }

  save(){
    const result = this.validateAll(this.props.examPage);
    if(!result){
      return;
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'examPage/saveResult',
      payload:this.props.examPage
    });
  }

  submit(){
    const result = this.validateAll(this.props.examPage);
    if(!result){
      return;
    }

    const { dispatch } = this.props;
    dispatch({
      type: 'examPage/submitResult',
      payload:this.props.examPage
    });
  }

  titleChange(e){
    this.props.dispatch({
      type: 'examPage/modifyTitle',
      payload: {
        title: e.target.value,
      },
    });
  }

  paperInfoChange(key1, key2, e){
    const paperInfo = JSON.parse(JSON.stringify(this.props.examPage.paperInfo));
    if(key2 === 'isChecked'){
      paperInfo[key1][key2] = e.target.checked;
    }
    else{
      paperInfo[key1][key2] = e.target? e.target.value : e;
    }

    this.props.dispatch({
      type: 'examPage/modifyPaperInfo',
      payload: {
        paperInfo,
      },
    });
  }

  addQuiz(index, parentIndex, value){

    // this.props.dispatch({
    //   type: 'examPage/addQuiz',
    //   questionType: Number(value),
    //   index:index
    // });
    // if(Number(value) === 1){
    //   this.setState({
    //     currentSubject: JSON.parse(JSON.stringify(papaerSetting[1])),
    //     currentIndex: index,
    //     parentIndex: parentIndex
    //   },function(){
    //     this.setState({
    //       isAddModalVisible:true
    //     })
    //   })
    // }

    const obj = this.state.popoverVisible;
    obj[index]= false;

    this.setState({
      popoverVisible:obj,
      currentSubject: JSON.parse(JSON.stringify(papaerSetting[Number(value)])),
      currentIndex: index,
      parentIndex: parentIndex
    },function(){
      this.setState({
        isAddModalVisible:true
      })
    })


  }
  deleteQuiz(index, parentIndex){
    message.success("删除成功!");
    this.props.dispatch({
      type: 'examPage/deleteQuiz',
      index:index,
      parentIndex: parentIndex
    });
  }

  modifyQuizGeneral(key, e){
    let currentSubject = this.state.currentSubject;
    currentSubject[key] = e.target.value
    this.setState({
      currentSubject:currentSubject
    })
  }

  modifyQuizSelectGeneral(key, value){
    let currentSubject = this.state.currentSubject;
    currentSubject[key] =value
    this.setState({
      currentSubject:currentSubject
    })
  }

 modifyQuizDescription(e){
   let currentSubject = this.state.currentSubject;
   currentSubject.description = e.target.value
   this.setState({
     currentSubject:currentSubject
   })
 }

  modifyQuizContent(type,value, e){
    let currentSubject = this.state.currentSubject;

    switch(type){
       case "1": currentSubject.content[0].text = e.target.value; break;
       case "2": currentSubject.content[1].url = value;
              currentSubject.content[1].width = value.length==0?value:value[0].width;
              currentSubject.content[1].height =  value.length==0?value:value[0].height;
              break;
       case "3-1": currentSubject.content[2].url = value;
                currentSubject.content[2].duration = value.length==0?value:value[0].duration;
                if(value.length==0){
                  currentSubject.content[2].autoPlay = false;
                    currentSubject.content[2].repeat = 0;
                }
                break;
       case "3-2":
                currentSubject.content[2].autoPlay = e;
                break;
       case "3-3":
                currentSubject.content[2].repeat = e;
                break;
       case "4":
                currentSubject.content[3].url = value;
                break;
       default: break;
    }
    this.setState({
      currentSubject:currentSubject
    })
  }


  modifyQuizOptions(optionsIndex,e){
    let currentSubject = this.state.currentSubject;
    currentSubject.options[optionsIndex].description = e.target.value
    this.setState({
      currentSubject:currentSubject
    })
  }

  durationUnionIsUnchangeble(e){
    let currentSubject = this.state.currentSubject;
    if(e.target.checked){
      currentSubject.duration = 10;
      currentSubject.isUnchangeable = true;
    }
    else{
      currentSubject.duration = 0;
    }

    this.setState({
      currentSubject:currentSubject
    })

  }

  getAnswerIndex(options){
    for(let i=0;i<options.length;i++){
      if(options[i].isRight){
        return i;
      }
    }
    return 100;
  }

  modifyQuizAnswer(value){
    let currentSubject = this.state.currentSubject;
    for(let i=0;i<currentSubject.options.length;i++){
      currentSubject.options[i].isRight = false;
    }
    if(value !== 100){
      currentSubject.options[value].isRight = true;
    }

    this.setState({
      currentSubject:currentSubject
    })
  }

  modifyQuizOptionsLayoutMode(value){
    let currentSubject = this.state.currentSubject;
    currentSubject.layoutMode =  value
    this.setState({
      currentSubject:currentSubject
    })
  }

  showModal(index, parentIndex) {
   let temp;
   if(parentIndex == -1){
     temp = JSON.parse(JSON.stringify(this.props.examPage.quizes[index]));
   }
   else{
     temp = JSON.parse(JSON.stringify(this.props.examPage.quizes[parentIndex].quizes[index]));
   }


    this.setState({
      currentSubject: temp,
      currentIndex: index,
      parentIndex: parentIndex,
    },function(){
      this.setState({
        isModalVisible:true
      })
    })

  }

  handlePopoverVisibleChange( index , visible){
    const obj = this.state.popoverVisible;
    obj[index] = visible;

    this.setState({
      popoverVisible: obj
    });
  }

  handleOk() {
    let result = this.validateSingleQuiz(this.state.currentSubject);
    if(result){
      this.props.dispatch({
        type: 'examPage/modifyQuiz',
        index:this.state.currentIndex,
        parentIndex: this.state.parentIndex,
        value:this.state.currentSubject
      });
      this.setState({
        isModalVisible:false
      })
    }
  }

  handleCancel(){
    this.setState({isModalVisible:false})
  }

  handleAddOk() {
    let result = this.validateSingleQuiz(this.state.currentSubject);

    if(result){
      this.props.dispatch({
        type: 'examPage/addQuiz',
        index:this.state.currentIndex,
        parentIndex: this.state.parentIndex,
        value:this.state.currentSubject
      });

      this.setState({
        isAddModalVisible:false
      })
    }

  }

  handleAddCancel(){
    this.setState({isAddModalVisible:false})
  }

  questionType (index, parentIndex){
    return (
      <div>
        {/*<Select  style={{ width: 120 }}   onSelect={this.addQuiz.bind(this,index, parentIndex)} >*/}
        {/*  <Select.Option value="1">单选题</Select.Option>*/}
        {/*  <Select.Option value="6">描述</Select.Option>*/}
        {/*  <Select.Option value="0">复合题</Select.Option>*/}
        {/*</Select>*/}
        <div  className={styles.popoverRow} onClick={this.addQuiz.bind(this,index, parentIndex, "1")}>单选题</div>
        <div  className={styles.popoverRow} onClick={this.addQuiz.bind(this,index, parentIndex, "6")}>描述</div>
        <div  className={styles.popoverRow} onClick={this.addQuiz.bind(this,index, parentIndex, "0")}>复合题</div>
        <div  className={styles.popoverRow} onClick={this.addQuiz.bind(this,index, parentIndex, "7")}>建模题</div>
      </div>
    )
  }

  addOptions(){
    let currentSubject = this.state.currentSubject;
    currentSubject.options.push(JSON.parse(JSON.stringify(papaerSetting[1].options[0])))
    currentSubject.options.map(function(item){
      const itemNew = item;
      itemNew.isRight = false;
      return itemNew
    })
    this.setState({
      currentSubject:currentSubject
    })
  }

  deleteOptions(index){
    let currentSubject = this.state.currentSubject;
    currentSubject.options.splice(index, 1);
    currentSubject.options.map(function(item){
      const itemNew = item;
      itemNew.isRight = false;
      return itemNew
    })
    this.setState({
      currentSubject:currentSubject
    })
  }

validateSingleQuiz(current){
  if( current.description &&  current.description.length > 1024){
      message.error("题目的描述不能超过1024个字符");
      return false;
  }
  if(current.duration && !/^\d+$/.test(current.duration)){
    message.error("时长必须是整数");
    return false;
  }
  if(current.score && !/^(([1-9]\d{1,3}(\.\d{1,2})?)|\d((\.\d{1,2})?)|10000|10000.0|10000.00)$/.test(current.score)){
    message.error("分值必须是0-10000的整数或两位小数");
    return false;
  }
  if(current.content && current.content.length > 0){
    for (let index in current.content){
      if(current.content[index].type === 1 && current.content[index].text.length > 1024 ){
        message.error("题目的题干不能超过1024个字符");
        return false;
      }
    }
  }
  for (let index in current.options){
    if(current.options[index].description.length > 1024 ){
      message.error("题目的选项不能超过1024个字符");
      return false;
    }
  }
  return true;
}

validateAll(info){
  if(info.title && info.title.length >= 50 ){
    message.error("考试名称要小于50个字符");
    return false;
  }
    return true;
}

generateQuizContentInMainPage(){
  let res =[
    <div className={styles.firstPara} key={-1}>
      <Row span={24} className={styles.formRow}>
       <Input placeholder="请输入试卷名称" className={styles.firParaInput} bordered={false} onChange={this.titleChange.bind(this)} value={this.props.examPage.title}/>
      </Row>
      <Popover content={this.questionType.bind(this,-1, -1)} title="选择题型" trigger="click" visible={this.state.popoverVisible["-1"]} onVisibleChange={this.handlePopoverVisibleChange.bind(this,"-1")}>
        <img src={add} className={styles.buttonAdd} data-index ={-1} />
      </Popover>
    </div>
  ];


  const contentInfo = (index) =>  {
    let contentInfoArray = [] ;

    for(let i=0; i<this.props.examPage.quizes[index].content.length;i++){
      switch(this.props.examPage.quizes[index].content[i].type){
        case 1:
           contentInfoArray.push(this.props.examPage.quizes[index].content[i].text == "" ?<div key={i}></div>: <div className={styles.cardRow} key={i}>题干: {this.props.examPage.quizes[index].content[i].text}</div>);
           break;
        case 2:
           contentInfoArray.push(this.props.examPage.quizes[index].content[i].url.length == 0 ?<div key={i}></div>: <div className={styles.cardRow} key={i}>图片: <img src={this.props.examPage.quizes[index].content[i].url[0].url} width="200" /></div>);
           break;
        case 3:
           contentInfoArray.push( this.props.examPage.quizes[index].content[i].url.length == 0 ?<div key={i}></div>:  <div className={styles.cardRow} key={i}>
             <span style={{marginRight:"16px"}}>音频: <a target="_blank" href={this.props.examPage.quizes[index].content[i].url[0].url}> {this.props.examPage.quizes[index].content[i].url[0].name} </a></span>
             <span style={{marginRight:"16px"}}>播放方式: {this.props.examPage.quizes[index].content[i].autoPlay?"自动播放":"手动播放" } </span>
             <span style={{marginRight:"16px"}}>播放次数: {this.props.examPage.quizes[index].content[i].repeat != 0? this.props.examPage.quizes[index].content[i].repeat :"无限"} </span>
             </div>);
           break;
      }
    }
    return <div>
      {this.props.examPage.quizes[index].description?  <div className={styles.cardRow} >{"题目描述:"+this.props.examPage.quizes[index].description}</div>:<div></div>}
      {contentInfoArray}
    </div>
  };

  const contentInfoInner= (index,parentIndex) =>{

    let contentInfoArray = [] ;

    for(let i=0; i<this.props.examPage.quizes[parentIndex].quizes[index].content.length;i++){
      switch(this.props.examPage.quizes[parentIndex].quizes[index].content[i].type){
        case 1:
           contentInfoArray.push(this.props.examPage.quizes[parentIndex].quizes[index].content[i].text == "" ?<div key={i}></div>: <div className={styles.cardRow} key={i}>题干: {this.props.examPage.quizes[parentIndex].quizes[index].content[i].text}</div>);
           break;
        case 2:
           contentInfoArray.push(this.props.examPage.quizes[parentIndex].quizes[index].content[i].url.length == 0 ?<div key={i}></div>: <div className={styles.cardRow} key={i}>图片: <img src={this.props.examPage.quizes[parentIndex].quizes[index].content[i].url[0].url} width="200" /></div>);
           break;
        case 3:
           contentInfoArray.push(  this.props.examPage.quizes[parentIndex].quizes[index].content[i].url.length == 0 ?<div key={i}></div>: <div className={styles.cardRow} key={i}>
             <span style={{marginRight:"16px"}}>音频: <a target="_blank" href={this.props.examPage.quizes[parentIndex].quizes[index].content[i].url[0].url}> {this.props.examPage.quizes[parentIndex].quizes[index].content[i].url[0].name} </a></span>
             <span style={{marginRight:"16px"}}>播放方式: {this.props.examPage.quizes[parentIndex].quizes[index].content[i].autoPlay?"自动播放":"手动播放" } </span>
             <span style={{marginRight:"16px"}}>播放次数: {this.props.examPage.quizes[parentIndex].quizes[index].content[i].repeat != 0? this.props.examPage.quizes[parentIndex].quizes[index].content[i].repeat :"无限"} </span>
             </div>);
           break;
      }
    }

    return <div>
      {this.props.examPage.quizes[parentIndex].quizes[index].description?  <div className={styles.cardRow} >{"题目描述:"+this.props.examPage.quizes[parentIndex].quizes[index].description}</div>:<div></div>}
      {contentInfoArray}
    </div>
  }

  for(let i = 0; i < this.props.examPage.quizes.length; i++) {
    switch (this.props.examPage.quizes[i].type) {
      case 1:
      let answer =this.getAnswerIndex(this.props.examPage.quizes[i].options);
       res.push(
        <Card title={i+1} bordered={false} className={styles.quizCard} key={i} quiz-index={i} parent-index={-1}>
          <div>
          {contentInfo(i)}
            <Radio.Group value={answer} className={styles.radioGroup +" "+ styles.cardRow}>
              {
                this.props.examPage.quizes[i].options.map(function(item,index){
                  return <Radio value={index} key={index}>{item.description}</Radio>
                })
              }

            </Radio.Group>

            <span className={styles.quizConfig}>分值: {this.props.examPage.quizes[i].score}</span>
            <span className={styles.quizConfig}>答题时间: {this.props.examPage.quizes[i].duration} s</span>
            <span className={styles.quizConfig}>禁止修改: {this.props.examPage.quizes[i].isUnchangeable ? "是":"否"}</span>
            <span className={styles.quizConfig}>答案排列: {this.props.examPage.quizes[i].layoutMode == 0 ? "横向":"纵向"}</span>

          </div>
          <img src={deleteIcon} className={styles.buttonDelete} data-index ={i} onClick={this.deleteQuiz.bind(this,i,-1)} />
          <Popover content={this.questionType.bind(this,i, -1)} title="选择题型" trigger="click" visible={this.state.popoverVisible[""+i]} onVisibleChange={this.handlePopoverVisibleChange.bind(this,""+i)} key={i}>
          <img src={add}  className={styles.buttonAdd} data-index ={i}  />
          </Popover>
          <a type="primary" className={styles.buttonEdit} data-index ={i} onClick={this.showModal.bind(this,i,-1)} >编辑</a>
        </Card>
      )
        break;
      case 6:
        res.push(
         <Card title={i+1} bordered={false} className={styles.quizCard} key={i} quiz-index={i} parent-index={-1}>
           <div>
           {contentInfo(i)}
           <span className={styles.quizConfig}>停留时间: {this.props.examPage.quizes[i].duration == 0? "无限":this.props.examPage.quizes[i].duration +' s'}</span>
           <span className={styles.quizConfig}>禁止修改: {this.props.examPage.quizes[i].isUnchangeable ? "是":"否"}</span>
           </div>
           <img  src={deleteIcon} className={styles.buttonDelete} data-index ={i} onClick={this.deleteQuiz.bind(this,i,-1)} />
           <Popover content={this.questionType.bind(this,i, -1)} title="选择题型" trigger="click" visible={this.state.popoverVisible[""+i]} onVisibleChange={this.handlePopoverVisibleChange.bind(this,""+i)} key={i}>
           <img src={add}  className={styles.buttonAdd} data-index ={i}  />
           </Popover>
           <a type="primary" className={styles.buttonEdit} data-index ={i} onClick={this.showModal.bind(this,i,-1)} >编辑</a>
         </Card>
       )
        break;
      case 0:
         let innerQuiz = this.props.examPage.quizes[i].quizes;
         let innerQuizEveryQuestion =innerQuiz.length ==0?
           <div>
                <a type="primary" className={styles.buttonNoBackground} data-index ={0}  parent-index={i} onClick={this.addQuiz.bind(this,0, i, 1)}>小题添加</a>
           </div>
         :[];

         for(let inerIndex =0; inerIndex<innerQuiz.length; inerIndex++){
           switch (innerQuiz[inerIndex].type) {
             case 1:
              let answerInner =this.getAnswerIndex(innerQuiz[inerIndex].options);
              innerQuizEveryQuestion.push(
                <Card title={inerIndex+1} bordered={false} className={styles.quizCard} key={inerIndex} quiz-index={inerIndex} parent-index={i}>
                  <div>
                  {contentInfoInner(inerIndex, i)}
                    <Radio.Group value={answerInner} className={styles.radioGroup +" "+ styles.cardRow}>
                      {
                        this.props.examPage.quizes[i].quizes[inerIndex].options.map(function(item,index){
                          return <Radio value={index} key={index}>{item.description}</Radio>
                        })
                      }
                    </Radio.Group>
                    <span className={styles.quizConfig}>分值: { this.props.examPage.quizes[i].quizes[inerIndex].score}</span>
                    <span className={styles.quizConfig}>答题时间: { this.props.examPage.quizes[i].quizes[inerIndex].duration} s</span>
                    <span className={styles.quizConfig}>禁止修改: { this.props.examPage.quizes[i].quizes[inerIndex].isUnchangeable ? "是":"否"}</span>
                    <span className={styles.quizConfig}>答案排列: { this.props.examPage.quizes[i].quizes[inerIndex].layoutMode == 0 ? "横向":"纵向"}</span>
                  </div>
                  <a className={styles.buttonNoBackground} data-index ={inerIndex} onClick={this.deleteQuiz.bind(this,inerIndex,i)}>小题删除</a>
                  <a type="primary" className={styles.buttonNoBackground} data-index ={inerIndex} onClick={this.addQuiz.bind(this,inerIndex, i, 1)} >小题添加</a>
                  <a type="primary" className={styles.buttonNoBackground} data-index ={inerIndex} onClick={this.showModal.bind(this,inerIndex,i)} >小题编辑</a>
                </Card>

            );
              break;

             default:break;
           }

         }

          res.push(
               <Card title={i+1} bordered={false} className={styles.quizCard} key={i} quiz-index={i} parent-index={-1}>
                 <div>
                 {contentInfo(i)}

                 <span className={styles.quizConfig}>答题时间: {this.props.examPage.quizes[i].duration} s</span>
                 <span className={styles.quizConfig}>禁止修改: {this.props.examPage.quizes[i].isUnchangeable ? "是":"否"}</span>
                 <span className={styles.quizConfig}>页面排布: {this.props.examPage.quizes[i].layoutMode == 0 ? "左右结构":"上下结构"}</span>

                 </div>
                    <div className={styles.innerQuizArea}>
                        {innerQuizEveryQuestion}
                    </div>
                 <img src={deleteIcon} className={styles.buttonDelete} data-index ={i} onClick={this.deleteQuiz.bind(this,i,-1)} />
                 <Popover content={this.questionType.bind(this,i, -1)} title="选择题型" trigger="click" visible={this.state.popoverVisible[""+i]} onVisibleChange={this.handlePopoverVisibleChange.bind(this,""+i)} key={i}>
                 <img src={add}   className={styles.buttonAdd}data-index ={i}  />
                 </Popover>
                 <a type="primary" className={styles.buttonEdit} data-index ={i} onClick={this.showModal.bind(this,i,-1)} >编辑</a>
               </Card>
             )
             break;
      case 7:
        res.push(
          <Card title={i+1} bordered={false} className={styles.quizCard} key={i} quiz-index={i} parent-index={-1}>
            <div>
              {contentInfo(i)}
              {this.props.examPage.quizes[i].files[0].url.length == 0 ?<div key={i}></div>:  <div className={styles.cardRow} key={i}>
              <span style={{marginRight:"16px"}}>题目文件: <a target="_blank" href={this.props.examPage.quizes[i].files[0].url[0].url}> {this.props.examPage.quizes[i].files[0].url[0].name} </a></span>
              </div>}
              <span className={styles.quizConfig}>停留时间: {this.props.examPage.quizes[i].duration == 0? "无限":this.props.examPage.quizes[i].duration +' s'}</span>
              <span className={styles.quizConfig}>禁止修改: {this.props.examPage.quizes[i].isUnchangeable ? "是":"否"}</span>
              <span className={styles.quizConfig}>分值: {this.props.examPage.quizes[i].score}</span>
            </div>
            <img  src={deleteIcon} className={styles.buttonDelete} data-index ={i} onClick={this.deleteQuiz.bind(this,i,-1)} />
            <Popover content={this.questionType.bind(this,i, -1)} title="选择题型" trigger="click" visible={this.state.popoverVisible[""+i]} onVisibleChange={this.handlePopoverVisibleChange.bind(this,""+i)} key={i}>
              <img src={add}  className={styles.buttonAdd} data-index ={i}  />
            </Popover>
            <a type="primary" className={styles.buttonEdit} data-index ={i} onClick={this.showModal.bind(this,i,-1)} >编辑</a>
          </Card>
        )
        break;
      default: break;
    }
  }


  return res;
}

generateModalInfo(){
  const { TextArea } = Input;
  const { Option } = Select;

  let modalInfo = (<div></div>);
  let self =this;

  const propsImg = {
    action: API_SERVER + 'upload-service/api/upload/img',
    headers: {
      'x-auth-token': localStorage.getItem("token"),
    },
    name:"img",
    listType:"picture-card",
    accept:"image/*",
    beforeUpload: file => {
      if (file.size >= (30*1024*1024)) {
        message.error(`${file.name} should less than 30MB`);
      }
      return file.size < (30*1024*1024) ? true : Upload.LIST_IGNORE;
    },
    onChange(info) {
      let fileList = [...info.fileList];
      // 1. Limit the number of uploaded files
      // Only to show two recent uploaded files, and old ones will be replaced by the new
      fileList = fileList.slice(-1);

      // 2. Read from response and show file link
      fileList = fileList.map(file => {
        if (file.response && file.response.status == 'ok') {
          // Component will show file.url as link
          file.url = file.response.data.url;
          file.width = file.response.data.width;
          file.height = file.response.data.height;
        }
        return file;
      });


      self.modifyQuizContent("2",fileList);
    },
  };

  const propsAudio = {
    action: API_SERVER + 'upload-service/api/upload/audio',
    headers: {
      'x-auth-token': localStorage.getItem("token"),
    },
    name:"audio",
    accept:".mp3,.wmv",
    beforeUpload: file => {
      if (file.size >= (30*1024*1024)) {
        message.error(`${file.name} should less than 30MB`);
      }
      return file.size < (30*1024*1024) ? true : Upload.LIST_IGNORE;
    },
    onChange(info) {
      let fileList = [...info.fileList];

      // 1. Limit the number of uploaded files
      // Only to show two recent uploaded files, and old ones will be replaced by the new
      fileList = fileList.slice(-1);

      // 2. Read from response and show file link
      fileList = fileList.map(file => {
        if (file.response && file.response.status == 'ok') {
          // Component will show file.url as link
          file.url = file.response.data.url;
          file.duration =  file.response.data.duration;
        }
        return file;
      });


      self.modifyQuizContent("3-1",fileList);
    },
  };

  const propsFile = {
    action: API_SERVER + 'upload-service/api/upload/file',
    headers: {
      'x-auth-token': localStorage.getItem("token"),
    },
    name:"file",
    accept:".xls,.xlsx",
    beforeUpload: file => {
      if (file.size >= (30*1024*1024)) {
        message.error(`${file.name} should less than 30MB`);
      }
      return file.size < (30*1024*1024) ? true : Upload.LIST_IGNORE;
    },
    onChange(info) {
      let fileList = [...info.fileList];

      // 1. Limit the number of uploaded files
      // Only to show two recent uploaded files, and old ones will be replaced by the new
      fileList = fileList.slice(-1);

      // 2. Read from response and show file link
      fileList = fileList.map(file => {
        if (file.response && file.response.status == 'ok') {
          // Component will show file.url as link
          file.url = file.response.data.url;
        }
        return file;
      });


      self.modifyQuizSelectGeneral("files",[{url:fileList}]);
    },
  };

  let contentInfo = (Object.keys(this.state.currentSubject) != 0?
  <div>
    <Row>
      <Col  span={18} style={{paddingRight:"16px"}}>
        <p>题干：</p> <TextArea rows={4} placeholder="请输入题干" className={styles.editGroup} onChange={this.modifyQuizContent.bind(this,"1","")} value={this.state.currentSubject.content[0].text}/>
      </Col>
        <Col  span={6} style={{paddingLeft:"16px"}}>

        <p>图片：</p> <Upload   {...propsImg} fileList={this.state.currentSubject.content[1].url}>
          {this.state.currentSubject.content[1].url.length < 1 && '+ 上传'}
        </Upload>

        </Col>
    </Row>

    <Row  style={{marginBottom:"16px"}}>
       <Col  span={14} style={{paddingRight:"16px"}}>
        <p>音频：</p>  <Upload   {...propsAudio} fileList={this.state.currentSubject.content[2].url} >
            <Button icon={<UploadOutlined />}>添加音频</Button>
          </Upload>
        </Col>
        {this.state.currentSubject.content[2].url.length!=0? <Col  span={5} style={{paddingRight:"16px"}}>
         <p>控制：</p><Select  style={{ width: 100 }} onChange={this.modifyQuizContent.bind(this,"3-2","")} value={this.state.currentSubject.content[2].autoPlay}>
           <Select.Option value={true}>自动播放</Select.Option>
           <Select.Option value={false}>手动播放</Select.Option>
         </Select>
         </Col>:<div></div>}
         {this.state.currentSubject.content[2].url.length!=0? <Col  span={5} style={{paddingRight:"16px"}}>
          <p>循环：</p><Select  style={{ width: 100 }} onChange={this.modifyQuizContent.bind(this,"3-3","")} value={this.state.currentSubject.content[2].repeat}>
            <Select.Option value={0}>无限播放</Select.Option>
            <Select.Option value={1}>播放1次</Select.Option>
            <Select.Option value={2}>播放2次</Select.Option>
            <Select.Option value={3}>播放3次</Select.Option>
          </Select>
          </Col>:<div></div>}
    </Row>
  </div>
  :<div></div> );

  switch (this.state.currentSubject.type) {
    case 1: modalInfo = (
      <div>
        <p>题目描述：</p> <TextArea rows={4} placeholder="请输入题目描述" className={styles.editGroup} onChange={this.modifyQuizDescription.bind(this)} value={this.state.currentSubject.description}/>
        {contentInfo}

        <div style={{width:"800px","margin":"0px 0px 24px -24px","padding":"24px 24px 0px 24px",borderTop:"1px solid #f0f0f0",borderBottom:"1px solid #f0f0f0" }}>
          {
            this.state.currentSubject.options.map(function (item,index) {
              return <Row key={index}>
                <Col style={{margin:"4px 8px 0px 0px"}}>{optionMap[index]+": "}</Col>
                <Col style={{width:"678px"}}> <Input placeholder={"请输入选项"+(index+1)} className={styles.editGroup} onChange={self.modifyQuizOptions.bind(self,index)} value={self.state.currentSubject.options[index].description}/></Col>
                {
                  index >0 ? <Col style={{margin:"4px 0px 0px 18px",cursor:'pointer'}} ><span style={{color:'rgb(255,116,96)'}} onClick={self.deleteOptions.bind(self,index)}>删除</span></Col>:""
                }

              </Row>
            })
          }
          {
            this.state.currentSubject.options.length <=9 ? <div  className={styles.addOption} onClick={self.addOptions.bind(self)}>+ 添加选项</div> :""
          }
        </div>
        <p style={{ width: 80,display:'inline' }}>正确答案：</p>
        <Select  style={{ width: 100 }} onChange={this.modifyQuizAnswer.bind(this)} value={optionMap[this.getAnswerIndex(this.state.currentSubject.options)]}>
          <Select.Option value={100} key={100}>{optionMap[100]}</Select.Option>
          {
            this.state.currentSubject.options.map(function (item,index) {
               return   <Select.Option value={index} key={index}>{optionMap[index]}</Select.Option>
            })
          }
        </Select>
        <Checkbox style={{ width: 24,marginLeft:'24px'  }} onChange={this.durationUnionIsUnchangeble.bind(this)} checked={!(this.state.currentSubject.duration === 0 || this.state.currentSubject.duration === '0' )}> </Checkbox>
        <p style={{ width: 80,display:'inline' }}>答题时间：</p>
        <Input  disabled={ (this.state.currentSubject.duration === 0 || this.state.currentSubject.duration === '0' ) } style={{ width: 80}} placeholder="" className={styles.editGroup} onChange={this.modifyQuizGeneral.bind(this,'duration')} value={this.state.currentSubject.duration}/>
        <p style={{ width: 80,display:'inline' ,marginLeft:'24px' }}>禁止更改：</p>
        <Select disabled={ !(this.state.currentSubject.duration === 0 || this.state.currentSubject.duration === '0' ) }  style={{ width: 100 }} onChange={this.modifyQuizSelectGeneral.bind(this,'isUnchangeable')} value={this.state.currentSubject.isUnchangeable}>
          <Select.Option value={true}>是</Select.Option>
          <Select.Option value={false}>否</Select.Option>
        </Select>
        <br/>
        <p style={{ width: 80,display:'inline' }}>分值：</p>
        <Input  style={{ width: 80 }} placeholder="" className={styles.editGroup} onChange={this.modifyQuizGeneral.bind(this,'score')} value={this.state.currentSubject.score}/>
        <p style={{ width: 80,display:'inline',marginLeft:'24px' }}>答案排列：</p>
        <Select  style={{ width: 100 }} onChange={this.modifyQuizOptionsLayoutMode.bind(this)} value={this.state.currentSubject.layoutMode}>
          <Select.Option value={0}>横向</Select.Option>
          <Select.Option value={1}>纵向</Select.Option>
        </Select>
      </div>
    )
      break;
    case 6: modalInfo = (
      <div>
        {contentInfo}
        <p style={{ width: 80,display:'inline',marginLeft:'24px' }}>停留时间：</p>
        <Input  style={{ width: 100 }} onChange={this.modifyQuizGeneral.bind(this,'duration')} value={this.state.currentSubject.duration} />

        <p style={{ width: 80,display:'inline',marginLeft:'24px' }}>禁止修改：</p>
        <Select  style={{ width: 100 }} onChange={this.modifyQuizSelectGeneral.bind(this,'isUnchangeable')} value={this.state.currentSubject.isUnchangeable}>
          <Select.Option value={true}>是</Select.Option>
          <Select.Option value={false}>否</Select.Option>
        </Select>
      </div>
    )
      break;
    case 0:
      modalInfo = (
        <div>
          <p>题目描述：</p> <TextArea rows={4} placeholder="请输入题目描述" className={styles.editGroup} onChange={this.modifyQuizDescription.bind(this)} value={this.state.currentSubject.description}/>
          {contentInfo}
          <Checkbox style={{ width: 24,marginLeft:'24px'  }} onChange={this.durationUnionIsUnchangeble.bind(this)} checked={!(this.state.currentSubject.duration === 0 || this.state.currentSubject.duration === '0' )}> </Checkbox>
          <p style={{ width: 80,display:'inline' }}>答题时间：</p>
          <Input disabled={ (this.state.currentSubject.duration === 0 || this.state.currentSubject.duration === '0' ) } style={{ width: 80}} placeholder="" className={styles.editGroup} onChange={this.modifyQuizGeneral.bind(this,'duration')} value={this.state.currentSubject.duration}/>
          <p style={{ width: 80,display:'inline' ,marginLeft:'24px' }}>禁止更改：</p>
          <Select disabled={ !(this.state.currentSubject.duration === 0 || this.state.currentSubject.duration === '0' ) }  style={{ width: 100 }} onChange={this.modifyQuizSelectGeneral.bind(this,'isUnchangeable')} value={this.state.currentSubject.isUnchangeable}>
            <Select.Option value={true}>是</Select.Option>
            <Select.Option value={false}>否</Select.Option>
          </Select>
          <p style={{ width: 80,display:'inline',marginLeft:'24px' }}>页面排布：</p>
          <Select  style={{ width: 100 }} onChange={this.modifyQuizSelectGeneral.bind(this,'layoutMode')} value={this.state.currentSubject.layoutMode}>
            <Select.Option value={0}>左右结构</Select.Option>
            <Select.Option value={1}>上下结构</Select.Option>
          </Select>
        </div>
      );
      break;
    case 7:
      modalInfo = (
        <div>
          {contentInfo}
          <Row  style={{marginBottom:"16px"}}>
            <Col  span={14} style={{paddingRight:"16px"}}>
              <p>题目文件：</p>  <Upload   {...propsFile} fileList={this.state.currentSubject.files[0].url} >
              <Button icon={<UploadOutlined />}>添加题目文件</Button>
            </Upload>
            </Col>
          </Row>
          <p style={{ width: 80,display:'inline',marginLeft:'24px' }}>停留时间：</p>
          <Input  style={{ width: 100 }} onChange={this.modifyQuizGeneral.bind(this,'duration')} value={this.state.currentSubject.duration} />

          <p style={{ width: 80,display:'inline',marginLeft:'24px' }}>禁止修改：</p>
          <Select  style={{ width: 100 }} onChange={this.modifyQuizSelectGeneral.bind(this,'isUnchangeable')} value={this.state.currentSubject.isUnchangeable}>
            <Select.Option value={true}>是</Select.Option>
            <Select.Option value={false}>否</Select.Option>
          </Select>

          <p style={{ width: 80,display:'inline', marginLeft:'24px' }}>分值：</p>
          <Input  style={{ width: 80 }} placeholder="" className={styles.editGroup} onChange={this.modifyQuizGeneral.bind(this,'score')} value={this.state.currentSubject.score}/>
        </div>
      );
      break;


    default:modalInfo = (<div></div>);break;
  }


  return modalInfo;
}

  render() {

    const {
      loading,
    } = this.props;


    const content = (
      <div className={styles.pageHeaderContent}>
        <span className={styles.title}>{this.props.match.params.id?"编辑试题":'创建试题'}</span>
           <Button type="primary" className={styles.buttonGroups} onClick={this.submit.bind(this)} style={{marginLeft:"24px"}}>提交</Button>
           <Button type="primary" className={styles.buttonGroups} onClick={this.save.bind(this)} >保存</Button>
      </div>
    );




    let modalInfo = this.generateModalInfo();

    let res = this.generateQuizContentInMainPage();


    return (
      <Spin spinning={loading?loading:false}>
        <PageContainer content={content} >
          <Row className={styles.quizContainer}  >
            <Col className={styles.quizMain} span={6}>
              <Tabs defaultActiveKey="1" onChange={callback} centered>
                <TabPane tab="试卷信息" key="1">
                  <Row span={24} className={styles.formRow}>
                    <Col span={8} style={{marginTop:'4px'}}>
                      <Checkbox  onChange={this.paperInfoChange.bind(this,'duration', 'isChecked')} checked={this.props.examPage.paperInfo.duration.isChecked}> </Checkbox>
                      答题时间:
                    </Col>
                    <Col span={8} ><Input placeholder="" onChange={this.paperInfoChange.bind(this,'duration', 'value')} value={this.props.examPage.paperInfo.duration.value}/></Col>
                    <Col span={6} style={{marginTop:'4px',marginLeft:"16px"}}>秒</Col>
                  </Row>
                  <Row span={24} className={styles.formRow}>
                    <Col span={10} style={{marginTop:'4px'}}>
                      <Checkbox  onChange={this.paperInfoChange.bind(this,'layoutMode', 'isChecked')} checked={this.props.examPage.paperInfo.layoutMode.isChecked}> </Checkbox>
                     统一答案排列:
                    </Col>
                    <Col span={8} >
                      <Select  style={{ width: 100 }} onChange={this.paperInfoChange.bind(this,'layoutMode', 'value')} value={this.props.examPage.paperInfo.layoutMode.value}>
                        <Select.Option value={0}>横向</Select.Option>
                        <Select.Option value={1}>纵向</Select.Option>
                      </Select>
                    </Col>
                  </Row>
                  <Row span={24} className={styles.formRow}>
                    <Col span={20} style={{marginTop:'4px'}}>
                      <Checkbox  onChange={this.paperInfoChange.bind(this,'isUnchangeable', 'isChecked')} checked={this.props.examPage.paperInfo.isUnchangeable.isChecked}> </Checkbox>
                      强制所有题目禁止更改
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tab="题目大纲" key="2" disabled>

                </TabPane>
              </Tabs>
            </Col>
            <Col className={styles.quizInfo} span={17} offset={1} >
              {res}
            </Col>
          </Row>
          <Modal title="编辑题目" className={styles.editModal} width={800}  okText={"保存"} cancelText={"取消"} visible={this.state.isModalVisible} onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)} closable={false} maskClosable={false} >
            {modalInfo}
          </Modal>
          <Modal title="添加题目" className={styles.editModal} width={800}  okText={"保存"} cancelText={"取消"} visible={this.state.isAddModalVisible} onOk={this.handleAddOk.bind(this)} onCancel={this.handleAddCancel.bind(this)} closable={false} maskClosable={false} >
            {modalInfo}
          </Modal>
        </PageContainer>
      </Spin>
    );
  }
}

export default connect(({ examPage, loading }) => ({
  examPage,
  loading: loading.models.examPage,
}))(ExamPage);
