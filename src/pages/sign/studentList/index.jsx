import {PlusOutlined, SearchOutlined} from '@ant-design/icons';
import {
  Button,
  Card,
  Input,
  List,
  Space,
  Table,
  Tabs,
  Typography,
  Modal,
  Dropdown,
  Menu,
  Form,
  Select,
  message,
  Row,
  Col,
  DatePicker,
  Upload, Spin, Checkbox
} from 'antd';
import React, { Component } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
import { UploadOutlined } from '@ant-design/icons';

const { Paragraph } = Typography;
import { history } from 'umi';
import moment from 'moment';
import { DownOutlined } from '@ant-design/icons';
import StudentList from "@/pages/test/main/components/studentList";

const { TextArea } = Input;
const { TabPane } = Tabs;

const statusMap = ['未报名','审核中','已报名','已结束','已退款','已延期']

class SignStudentList extends Component {
  formRef = React.createRef();
  formInfoRef = React.createRef();

  constructor() {
    super();
    this.state= {
      isModalVisible: false,
      isInfoModalVisible: false,
      isAdmissionModalVisible:false,
      isInfoCofirmModalVisible:false,
      onfirmLoading: false,
      modalInitialValues:{},
      admissionModalValues:{},
      infoModalValues:{},
      id: -1,
      loadingModal:false,
      loadingModalInner:false
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
    this.setState({
      id: this.props.match.params.id
    },function(){
        dispatch({
          type: 'signStudentList/fetch',
          payload: {
            pagination:{
              current: this.props.pagination.current,
              pageSize: this.props.pagination.pageSize,
              total: this.props.pagination.total,
            },
            id:this.state.id
          },
        });

      dispatch({
        type: 'signStudentList/fetchInfo',
        payload: {
          id:this.state.id
        },
      });

      }
    )

  }

  editModal = async (record) => {

    this.setState({loadingModal:true});

    let self =this;

    const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign-up/certificate?'+new URLSearchParams({
      signUpId:self.state.id,
      userId:record.userId,
    }),
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      }).then(response => response.json()
    )

    // eslint-disable-next-line no-underscore-dangle
    let files_ = [];
    if(returnResponse && returnResponse.status === 'ok'){
      files_ =[{uid:'-1',name: returnResponse.data, status:'done', url: returnResponse.data }] ;;
    }


    const initialValues = {
      userName: record.userName,
      userId: record.userId,
      account:record.account,
      certificateStatus:record.certificateStatus,
      files:files_,
      record
    }
    for(let i =0; i< record.exams.length;i++){
      initialValues[record.exams[i].id] = record.exams[i].status
    }

    this.setState({
      modalInitialValues: initialValues,
      isModalVisible:true,
      loadingModal:false
    })
  }

  editAdmission = async(record) => {
    this.setState({loadingModal:true});

    let self =this;

    const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign-up/admission?'+new URLSearchParams({
        signUpId:self.state.id,
        userId:record.userId,
       }),
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      }).then(response => response.json()
    )

    // eslint-disable-next-line no-underscore-dangle
    let imgs_ = [];
    if(returnResponse && returnResponse.status === 'ok'){
      imgs_ =[{uid:'-1',name: returnResponse.data, status:'done', url: returnResponse.data }] ;;
    }

    const admissionModalValues={
      userName: record.userName,
      userId: record.userId,
      account:record.account,
      imgs: imgs_,
      record
    };

    this.setState({
      admissionModalValues: admissionModalValues,
      isAdmissionModalVisible:true,
      loadingModal:false
    })
  }

  editInfoModal(type,e){
    const initialValues = JSON.parse(JSON.stringify(this.props.info))
    initialValues.type= type;

    this.setState({
      infoModalValues: initialValues,
      isInfoModalVisible:true
    })
  }

  download = async() => {
    try{
      const response = await fetch(API_SERVER +  'backend-service/api/sign/user/csv/download?signUpId='+this.state.id,
        {
          method: 'GET', // *GET, POST, PUT, DELETE, etc.
          // mode: 'cors', // no-cors, *cors, same-origin
          // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'x-auth-token': localStorage.getItem("token"),
          },
          responseType: 'blob'
          // redirect: 'follow', // manual, *follow, error
          // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          // body: JSON.stringify(data) // body data type must match "Content-Type" header
        });


      // 将文件流转为blob对象，并获取本地文件链接
      response.blob().then((blob) => {
        const a = window.document.createElement('a');
        const downUrl = window.URL.createObjectURL(blob);// 获取 blob 本地文件连接 (blob 为纯二进制对象，不能够直接保存到磁盘上)
        const filename = response.headers.get('Content-Disposition').split('filename=')[1].split('.');
        a.href = downUrl;
        a.download = `${decodeURI(filename[0])}.${filename[1]}`;
        a.click();
        window.URL.revokeObjectURL(downUrl);
      });

    }
    catch(err)
    {
      console.log(err)
    }
  }


  handleTableChange = (pagination, filters, sorter,e) => {
    // console.log(pagination,filters, sorter)
    // this.fetch({
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   pagination,
    //   ...filters,
    // });

    //much action may activate table change,only handle paginate part
    if(e.action != 'paginate'){
      return;
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'signStudentList/fetch',
      payload: {
        pagination:{
          current: pagination.current,
          pageSize:  pagination.pageSize,
          total: pagination.total,
        },
        searchAccount: this.props.searchAccount,
        searchName:this.props.searchName,
        id:this.state.id
      },
    });
  };


  handleOk = async() =>{
    const { dispatch } = this.props;
    // const values = await this.formRef.current.validateFields().then(values=>values, err =>err);
    try{
      const values = await this.formRef.current.validateFields();

      const _exams = [];

      for(let i =0; i< this.state.modalInitialValues.record.exams.length;i++){
        _exams.push(
          {
            'id':this.state.modalInitialValues.record.exams[i].id,
            'status':values[this.state.modalInitialValues.record.exams[i].id]
          }
        )
      }

      const data = {
        "certificateStatus": values.certificateStatus,
        "exams": _exams,
        "signUpId": this.state.id,
        "userId": this.state.modalInitialValues.userId
      }

      await this.setState({onfirmLoading:true});

      const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign-up/user/status',
        {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem("token"),
          },
          // redirect: 'follow', // manual, *follow, error
          // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        }).then(response => response.json()
      )

      this.setState({onfirmLoading:false})

      if(returnResponse. code === 200){
        this.setState({isModalVisible:false},
          function(){
            dispatch({
              type: 'signStudentList/fetch',
              payload: {
                pagination:{
                  current: this.props.pagination.current,
                  pageSize: this.props.pagination.pageSize,
                  total: this.props.pagination.total,
                },
                searchAccount: this.props.searchAccount,
                searchName:this.props.searchName,
                id:this.state.id
              },
            });
          }
        )
      }
      else{
        message.error(returnResponse.message)
      }
    }
    catch(err)
    {
      console.log(err)
    }

  }

  handleCancel(){
    this.setState({isModalVisible:false})
  }

  handleAdmissionOk(){
    const { dispatch } = this.props;
    this.setState(
      {isAdmissionModalVisible:false},
      function(){
        dispatch({
          type: 'signStudentList/fetch',
          payload: {
            pagination:{
              current: this.props.pagination.current,
              pageSize: this.props.pagination.pageSize,
              total: this.props.pagination.total,
            },
            searchAccount: this.props.searchAccount,
            searchName:this.props.searchName,
            id:this.state.id
          },
        });
      });
  }

  handleInfoOk = async() =>{
    const { dispatch } = this.props;
    try{
      // const values = await this.formInfoRef.current.validateFields();

      const data = this.state.infoModalValues;


      await this.setState({onfirmLoading:true});

      const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign-up/info',
        {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem("token"),
          },
          // redirect: 'follow', // manual, *follow, error
          // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        }).then(response => response.json()
      )

      this.setState({onfirmLoading:false})

      if(returnResponse. code === 200){
        this.setState({isInfoModalVisible:false, isInfoCofirmModalVisible:false},
          function(){
            dispatch({
              type: 'signStudentList/fetchInfo',
              payload: {
                id:this.state.id
              },
            });
          }
        )
      }
      else{
        message.error(returnResponse.message)
      }
    }
    catch(err)
    {
      console.log(err)
    }

  }

  handleInfoCancel(){
    this.setState({isInfoModalVisible:false})
  }

  onClick = async( studentId, key ) => {
    const { dispatch } = this.props;
    // const values = await this.formRef.current.validateFields().then(values=>values, err =>err);
    try{

      const data = {
        "studentId": studentId,
        "signUpId": this.state.id,
        "status": key
      }

      const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign/user/update',
        {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem("token"),
          },
          // redirect: 'follow', // manual, *follow, error
          // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        }).then(response => response.json()
      )

      if(returnResponse. code === 200){
        this.setState({isModalVisible:false},
          function(){
            dispatch({
              type: 'signStudentList/fetch',
              payload: {
                pagination:{
                  current: this.props.pagination.current,
                  pageSize: this.props.pagination.pageSize,
                  total: this.props.pagination.total,
                },
                searchAccount: this.props.searchAccount,
                searchName:this.props.searchName,
                id:this.state.id
              },
            });
          }
        )
      }
      else{
        message.error(returnResponse.message)
      }
    }
    catch(err)
    {
      console.log(err)
    }
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button onClick={() => this.handleReset(clearFilters, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
  })

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    const { dispatch } = this.props;
    const payload_ = {
      pagination:this.props.pagination,
      id:this.state.id
    };

    if(dataIndex === 'account'){
      payload_.searchAccount =selectedKeys[0]?selectedKeys[0]:'';
      payload_.searchName = this.props.searchName;
    }else{
      payload_.searchName =selectedKeys[0]?selectedKeys[0]:'';
      payload_.searchAccount = this.props.searchAccount;
    }

    dispatch({
      type: 'signStudentList/fetch',
      payload: payload_,
    });
  };

  handleReset = (clearFilters,dataIndex)=> {
    clearFilters();
    const { dispatch } = this.props;
    const payload_ = {
      pagination:this.props.pagination,
      id:this.state.id
    };

    if(dataIndex === 'account'){
      payload_.searchAccount ='';
      payload_.searchName = this.props.searchName;
    }else{
      payload_.searchName = '';
      payload_.searchAccount = this.props.searchAccount;
    }


    dispatch({
      type: 'signStudentList/fetch',
      payload: payload_,
    });
  };

  renderInfoModal= ()=>{

    let defaultStartValue_, defaultEndValue_ ='';
    // eslint-disable-next-line default-case
    switch(this.state.infoModalValues.type){
      case 'admission':
        defaultStartValue_ = this.state.infoModalValues.admissionReleaseStart;
        defaultEndValue_ = this.state.infoModalValues.admissionReleaseEnd;
        break;
      case 'exam':
        defaultStartValue_ = this.state.infoModalValues.examReleaseStart;
        defaultEndValue_ = this.state.infoModalValues.examReleaseEnd;
        break;
      case 'score':
        defaultStartValue_ = this.state.infoModalValues.scoreReleaseStart;
        defaultEndValue_ = this.state.infoModalValues.scoreReleaseEnd;
        break;
    }

      return <div>
        <Form.Item
          label={'开始时间'}
          name={'startTime'}
          labelAlign={"left"}
          style={{marginLeft:'40px'}}
        >

          <DatePicker format="YYYY-MM-DD HH:mm:ss" onChange={this.changeInfoTime.bind(this, this.state.infoModalValues.type,'start')} defaultValue={ moment(defaultStartValue_)} showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }} /> <span style={{marginLeft:'16px'}}>UTC+08:00</span>

        </Form.Item>
        <Form.Item
          label={'结束时间'}
          name={'endTime'}
          labelAlign={"left"}
          style={{marginLeft:'40px'}}
        >

          <DatePicker format="YYYY-MM-DD HH:mm:ss" onChange={this.changeInfoTime.bind(this, this.state.infoModalValues.type, 'end')} defaultValue={ moment(defaultEndValue_)} showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }} /> <span style={{marginLeft:'16px'}}>UTC+08:00</span>

        </Form.Item>
      </div>;


  }

  changeInfoTime= (modalType, timeType,e) =>{
    // eslint-disable-next-line no-underscore-dangle
    const infoModalValues_ =  this.state.infoModalValues;
    // eslint-disable-next-line default-case
    switch(this.state.infoModalValues.type){
      case 'admission':
        if(timeType === 'start'){
          infoModalValues_.admissionReleaseStart = moment(e).valueOf();
        }
        else{
          infoModalValues_.admissionReleaseEnd = moment(e).valueOf();
        }
        break;
      case 'exam':
        if(timeType === 'start'){
          infoModalValues_.examReleaseStart = moment(e).valueOf();
        }
        else{
          infoModalValues_.examReleaseEnd = moment(e).valueOf();
        }
        break;
      case 'score':
        if(timeType === 'start'){
          infoModalValues_.scoreReleaseStart = moment(e).valueOf();
        }
        else{
          infoModalValues_.scoreReleaseEnd = moment(e).valueOf();
        }
        break;
    }

    this.setState({infoModalValues : infoModalValues_});

  }

  InfoCheckChange = async(type,e) => {
    // eslint-disable-next-line default-case
    const info_ = JSON.parse(JSON.stringify(this.props.info));
    const {dispatch} = this.props;

    switch(type){
      case 'admission':info_.admissionReleaseEnable = e.target.checked; break;
      case 'exam': info_.examReleaseEnable = e.target.checked;  break;
      case 'score': info_.scoreReleaseEnable = e.target.checked;  break;
    }

    await this.setState({loadingModal:true});

    const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign-up/info',
      {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(info_) // body data type must match "Content-Type" header
      }).then(response => response.json()
    )


    if(returnResponse. code === 200) {
      await dispatch({
        type: 'signStudentList/fetchInfo',
        payload: {
          id: this.state.id
        }
      })
    }
    this.setState({loadingModal:false});

  }

  // eslint-disable-next-line consistent-return
  renderInfoConfirmModal=()=>{

    // eslint-disable-next-line default-case
    switch(this.state.infoModalValues.type){
      case 'admission':
        return '准考证发放时间';
      case 'exam':
        return '考试激活时间';
      case 'score':
        return '成绩公布时间';
    }

  }

  checkExams(modalValues) {
    let exams = JSON.stringify(modalValues) !=="{}" ?modalValues.record.exams:{};
    let flag = true;
    for(let i=0; i< exams.length;i++){
      if(exams[i].status === 2 ){
        flag = false;
        break;
      }
    }
    return flag;
  }

  render() {


    const {
      list,
      loading,
      pagination,
      info,
    } = this.props;

    const self = this;
    const {onfirmLoading} = this.state;

    const columns = [
      {
        key:1,
        title: '账号',
        dataIndex: 'account',
        ...this.getColumnSearchProps('account'),
      },
       {
         key:2,
           title: '姓名',
         dataIndex: 'userName',
         ...this.getColumnSearchProps('name'),
       },
      ];


    if(list.length > 0){
      list[0].exams.map(function (item,index) {
        columns.push(
          {
            key:item.id,
            title:item.title,
            render: (text, record) => {
              return statusMap[record.exams[index].status]
              }
          }
        )
      })
    }

    columns.push(
      {
        key:6,
        title: '准考证',
        dataIndex: 'occupation',
        render: (text, record) => {
          return (
            record.isAdmissionUploaded ?
            <a className="ant-dropdown-link" onClick={e =>{e.preventDefault();this.editAdmission(record)} }>
            编辑
            </a>: <a className="ant-dropdown-link" onClick={e =>{e.preventDefault();this.editAdmission(record)} }>
                上传准考证
              </a>
          )
        }
      },
      {
        key:7,
        title: '证书',
        dataIndex: 'certificateStatus',
        render: (text, record) => {
          return (
            record.certificateStatus === 0?'未颁发':'已颁发'
          )
        }
      },
      {
        key:8,
        title: '操作',
        dataIndex: 'operations', render: (text, record) => {
          return (
              <a className="ant-dropdown-link" onClick={e =>{e.preventDefault();this.editModal(record)} }>
                修改状态
              </a>
          )
        }
      },
    );

    const rowExpand = record => {
      return <Row >
        <Col span={12} style={{paddingLeft:'48px'}}>
          {
            record.formAnswers.map(function(item,index){
              return index% 2 === 0? <p className={styles.expandRowText} key={index}>{`${item.text}: `+(item.answer?item.answer:'')}</p>: ''
            })
          }
        </Col>
        <Col span={12} style={{paddingLeft:'48px'}}>
          {
            record.formAnswers.map(function(item,index){
              return index% 2 === 1? <p className={styles.expandRowText} key={index}>{`${item.text}: `+(item.answer?item.answer:'')}</p>: ''
            })
          }
        </Col>
      </Row>
    }

    const propsAdmission= {
      action: API_SERVER + 'upload-service/api/upload/admission',
      headers: {
        'x-auth-token': localStorage.getItem("token"),
      },
      name:"file",
      // listType:"picture-card",
      data:{signUpId:this.state.id, userId:this.state.admissionModalValues.userId},
      accept:"image/*",
      beforeUpload: file => {
        if (file.size >= (30*1024*1024)) {
          message.error(`${file.name} should less than 30MB`);
        }
        return file.size < (30*1024*1024) ? true : Upload.LIST_IGNORE;
      },
      onChange(info) {
        let fileList = [...info.fileList];
        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-1);
        // 2. Read from response and show file link
        fileList = fileList.map(file => {
          if (file.response && file.response.status == 'ok') {
            // Component will show file.url as link
            file.url = file.response.data.url;
          }
          return file;
        });

        const tempState_ = self.state.admissionModalValues;
        tempState_.imgs = fileList

        self.setState({admissionModalValues: tempState_});
      },
      onRemove(file){
        return new Promise(
          async(resolve, reject) => {
            self.setState({loadingModalInner:true});

            try{
              const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign-up/admission?'+new URLSearchParams({
                signUpId:parseInt(self.state.id),
                userId:self.state.admissionModalValues.userId,
              }),
                {
                  method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
                  mode: 'cors', // no-cors, *cors, same-origin
                  // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                  // credentials: 'same-origin', // include, *same-origin, omit
                  headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': localStorage.getItem("token"),
                  },
                  // redirect: 'follow', // manual, *follow, error
                  // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                  // body: JSON.stringify({
                  //   signUpId:parseInt(self.state.id),
                  //   userId:self.state.admissionModalValues.userId,
                  // }) // body data type must match "Content-Type" header
                }).then(response => response.json()
              )

              if(returnResponse && returnResponse.status === 'ok'){
                self.setState({loadingModalInner:false});
                resolve(true);
              }
              else{
                message.error(returnResponse.message);
                resolve(false);
                self.setState({loadingModalInner:false});
              }
            }
            catch (e) {
              console.log(e)
              // message.error(e);
              self.setState({loadingModalInner:false});
            }

          }
        )
      }
    };

    const propsCertificate= {
      action: API_SERVER + 'upload-service/api/upload/certificate',
      headers: {
        'x-auth-token': localStorage.getItem("token"),
      },
      name:"file",
      // listType:"picture-card",
      data:{signUpId:this.state.id, userId:this.state.modalInitialValues.userId},
      accept:"image/*",
      beforeUpload: file => {
        if (file.size >= (30*1024*1024)) {
          message.error(`${file.name} should less than 30MB`);
        }
        return file.size < (30*1024*1024) ? true : Upload.LIST_IGNORE;
      },
      onChange(info) {
        let fileList = [...info.fileList];
        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-1);
        // 2. Read from response and show file link
        fileList = fileList.map(file => {
          if (file.response && file.response.status == 'ok') {
            // Component will show file.url as link
            file.url = file.response.data.url;
          }
          return file;
        });

        const tempState_ = self.state.modalInitialValues;
        tempState_.files = fileList;

        self.setState({modalInitialValues: tempState_});
      },
      onRemove(file){
        return new Promise(
          async(resolve, reject) => {
            self.setState({loadingModalInner:true});

            try{
              const returnResponse = await fetch(API_SERVER +'backend-service/api/sign-up/certificate?'+new URLSearchParams({
                signUpId:parseInt(self.state.id),
                userId:self.state.modalInitialValues.userId,
              }),
                {
                  method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
                  mode: 'cors', // no-cors, *cors, same-origin
                  // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                  // credentials: 'same-origin', // include, *same-origin, omit
                  headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': localStorage.getItem("token"),
                  },
                  // redirect: 'follow', // manual, *follow, error
                  // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                  // body: JSON.stringify({
                  //   signUpId:parseInt(self.state.id),
                  //   userId:self.state.modalInitialValues.userId,
                  // }) // body data type must match "Content-Type" header
                }).then(response => response.json()
              )

              if(returnResponse && returnResponse.status === 'ok'){
                self.setState({loadingModalInner:false});
                resolve(true);
              }
              else{
                message.error(returnResponse.message);
                resolve(false);
                self.setState({loadingModalInner:false});
              }
            }
            catch (e) {
              console.log(e)
              // message.error(e);
              self.setState({loadingModalInner:false});
            }

          }
        )
      }
    };

    const content = (
      <div className={styles.pageHeaderContent}>

      </div>
    );

    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };

    const nullData = {};


    return (
      <Spin spinning={this.state.loadingModal}>
        <PageContainer content={content} className={styles.containerWrap}>
        <Tabs defaultActiveKey="1" style={{marginTop:'-14px'}}>
          <TabPane tab="报名列表" key="1">
            <Table
              columns={columns}
              rowKey={record => record.userId}
              dataSource={list}
              pagination={pagination}
              loading={loading}
              onChange={this.handleTableChange}
              expandable={{
                expandedRowClassName:() => {return styles.expandRow},
                expandedRowRender:rowExpand
              }}
            />
            {/*<div className={styles.buttonArea}>*/}
            {/*  <Button type="primary"  className={styles.buttonLeft} onClick={()=>this.add()}>+添加考生</Button>*/}
            {/*  <Button type="primary"  className={styles.buttonRight} onClick={()=>this.download()}>下载CSV</Button>*/}
            {/*</div>*/}
          </TabPane>
          <TabPane tab="报名设置" key="2">
            <Row className={styles.infoContainer}  >
              <Col span={24}  style={{'textAlign':'left', marginLeft:"36px"}}>
                <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'0px'}} >
                  <Col style={{fontSize:'20px',fontWeight:'800'}}>基础信息</Col>
                </Row>
                <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}} >
                  <Col >考试名称: </Col>
                  <Col >{info.examNames? info.examNames.join('/'):''}</Col>
                </Row>
                <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}} >
                  <Col >报名开放时间: </Col>
                  <Col >{ moment(info.startTime).format('YYYY-MM-DD HH:mm:ss')  } - { moment(info.endTime).format('YYYY-MM-DD HH:mm:ss') } UTC+08:00</Col>
                </Row>
                <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}} >
                  <Col><Checkbox  onChange={this.InfoCheckChange.bind(this,'admission')} checked={info.admissionReleaseEnable}> </Checkbox></Col>
                  <Col >准考证发放时间: </Col>
                  <Col >{info.admissionReleaseStart?moment(info.admissionReleaseStart).format('YYYY-MM-DD HH:mm:ss'):'' } - {info.admissionReleaseEnd?moment(info.admissionReleaseEnd).format('YYYY-MM-DD HH:mm:ss'):'' }  UTC+08:00</Col>
                  <Col style={{marginLeft:'16px',color:'#419EFF',cursor:'pointer'}} onClick={()=>this.editInfoModal('admission')}>修改</Col>
                </Row>
                <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}} >
                  <Col><Checkbox  onChange={this.InfoCheckChange.bind(this,'exam')} checked={info.examReleaseEnable}> </Checkbox></Col>
                  <Col >考试激活时间: </Col>
                  <Col >{info.examReleaseStart?moment(info.examReleaseStart).format('YYYY-MM-DD HH:mm:ss'):'' } - {info.examReleaseEnd?moment(info.examReleaseEnd).format('YYYY-MM-DD HH:mm:ss'):'' }  UTC+08:00</Col>
                  <Col style={{marginLeft:'16px',color:'#419EFF',cursor:'pointer'}} onClick={()=>this.editInfoModal('exam')}>修改</Col>
                </Row>
                <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}} >
                  <Col><Checkbox  onChange={this.InfoCheckChange.bind(this,'score')} checked={info.scoreReleaseEnable}> </Checkbox></Col>
                  <Col >成绩公布时间: </Col>
                  <Col >{info.scoreReleaseStart?moment(info.scoreReleaseStart).format('YYYY-MM-DD HH:mm:ss'):'' } - {info.scoreReleaseEnd?moment(info.scoreReleaseEnd).format('YYYY-MM-DD HH:mm:ss'):'' }  UTC+08:00</Col>
                  <Col style={{marginLeft:'16px',color:'#419EFF',cursor:'pointer'}} onClick={()=>this.editInfoModal('score')}>修改</Col>
                </Row>
              </Col>
            </Row>
          </TabPane>
        </Tabs>
        <Modal title="编辑用户信息"
               className={styles.editModal} confirmLoading={onfirmLoading}
               width={650} okText={"确认"} cancelText={"取消"} visible={this.state.isModalVisible} onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)} closable={false} maskClosable={false} destroyOnClose={true} >
          <Spin spinning={this.state.loadingModalInner}>
            <div className={styles.formArea}>
            <Form
              {...layout}
              name="normal_login"
              className={styles.modalLabel}
              initialValues={this.state.modalInitialValues}
              ref={this.formRef}
            >
              <div style={{marginBottom:'32px'}}>
                <span style={{marginRight:'24px',marginLeft:'16px',fontWeight:'500',fontSize:'20px'}}>{this.state.modalInitialValues.userName}</span>
                <span style={{fontSize:'16px'}}>{this.state.modalInitialValues.account}</span>
              </div>
              {
                this.state.modalInitialValues.record && this.state.modalInitialValues.record.exams.map(function(item){
                    return   <Form.Item
                      label={item.title}
                      name={item.id}
                      key = {item.id}
                    >
                      {
                        item.status === 0 ?
                          <Select style={{width:'160px'}}>
                            <Select.Option value={0}>未报名</Select.Option>
                            <Select.Option value={2}>已报名</Select.Option>
                          </Select>:
                          <Select style={{width:'160px'}}>
                            <Select.Option value={2}>已报名</Select.Option>
                            <Select.Option value={4}>已退款</Select.Option>
                            <Select.Option value={5}>已延期</Select.Option>
                          </Select>
                      }
                    </Form.Item>
                })
              }

              <Form.Item label='证书状态' name='certificateStatus'>
                <Select style={{width:'160px'}}>
                  <Select.Option value={0}>未颁发</Select.Option>
                  <Select.Option value={1}>已颁发</Select.Option>
                </Select>

              </Form.Item>

              <div>
                <p style={{marginBottom:'12px',display:'inline-block',width:'33.333333%',textAlign:'right'}}>在线证书：</p>
                <Upload   {...propsCertificate} fileList={this.state.modalInitialValues.files} >
                  <Button style={{marginBottom:'12px'}} icon={<UploadOutlined />} >上传</Button>
                </Upload>
              </div>

            </Form>
          </div>
          </Spin>
        </Modal>

        <Modal title="准考证修改"
               className={styles.editInfoModal} confirmLoading={onfirmLoading}
               width={650} okText={"确认"} cancelText={"取消"} visible={this.state.isAdmissionModalVisible} onOk={()=>this.handleAdmissionOk()} onCancel={() =>this.setState({isAdmissionModalVisible:false})} closable={false} maskClosable={false} destroyOnClose={true} >
          <Spin spinning={this.state.loadingModalInner}>
            <div className={styles.formArea}>
                  <div style={{marginBottom:'32px',paddingTop:'32px'}}>
                    <span style={{marginRight:'24px',marginLeft:'40px',fontWeight:'500',fontSize:'20px'}}>{this.state.admissionModalValues.userName}</span>
                    <span style={{fontSize:'16px'}}>{this.state.admissionModalValues.account}</span>
                  </div>
                  <div style={{marginLeft:'40px', marginRight:'40px',marginBottom:'40px'}}>
                    <p style={{marginBottom:'12px'}}  style={{display:'inline'}}>准考证：</p>
                    <Upload   {...propsAdmission} fileList={this.state.admissionModalValues.imgs} disabled={ this.checkExams(this.state.admissionModalValues)}>
                      <Button style={{marginBottom:'12px'}} icon={<UploadOutlined />}  disabled={  this.checkExams(this.state.admissionModalValues)}>上传</Button>
                    </Upload>
                  </div>
            </div>
          </Spin>
        </Modal>


        <Modal title="信息修改"
               className={styles.editInfoModal} confirmLoading={onfirmLoading}
               width={650} okText={"确认"} cancelText={"取消"} visible={this.state.isInfoModalVisible} onOk={()=>this.setState({isInfoCofirmModalVisible:true})} onCancel={this.handleInfoCancel.bind(this)} closable={false} maskClosable={false} destroyOnClose={true} >
          <div className={styles.formArea}>
            <Form
              labelCol={ {span: 6} }
              wrapperCol={ {span: 18} }
              name="normal_login"
              className={styles.modalLabel}
              // initialValues={this.state.infoModalValues}
              ref={this.formInfoRef}
            >
              <div style={{marginBottom:'28px',paddingTop:'32px'}}>
                <span style={{marginRight:'24px',marginLeft:'40px',fontWeight:'500',fontSize:'20px'}}>{this.renderInfoConfirmModal()}</span>
              </div>

              {this.renderInfoModal()}


            </Form>
          </div>
        </Modal>

        <Modal title="信息确认"
               className={styles.editInfoModal} confirmLoading={onfirmLoading} centered={true}
               width={650} okText={"确认"} cancelText={"取消"} visible={this.state.isInfoCofirmModalVisible} onOk={this.handleInfoOk.bind(this)} onCancel={()=>this.setState({isInfoCofirmModalVisible:false})} closable={false} maskClosable={false} destroyOnClose={true} >
          <div style={{margin:'0px auto 16px auto',textAlign:'center',paddingTop:'26px'}}>请注意</div>
          <div style={{margin:'0px auto 16px auto',textAlign:'center'}}>您确认要修改{this.renderInfoConfirmModal()}</div>
        </Modal>
      </PageContainer>
      </Spin>
    );
  }
}

export default connect(({ signStudentList, loading }) => ({
  list: signStudentList.list,
  pagination: signStudentList.pagination,
  searchAccount: signStudentList.searchAccount,
  searchName: signStudentList.searchName,
  info: signStudentList.info,
  loading: loading.models.signStudentList,
}))(SignStudentList);
