import { queryFakeList, getInfo } from './service';

const Model = {
  namespace: 'signStudentList',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 5,
      total:0
    },
    id:-1,
    info:{},
    searchAccount:'',
    searchName:''
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'modifyTable',
        pagination: payload.pagination,
        searchAccount: payload.searchAccount,
        searchName: payload.searchName,
        id:payload.id
      });

      const response = yield call(queryFakeList, payload);
      if(response){
        yield put({
          type: 'queryList',
          list: response.code == 200 ? response.data.list : [],
          total:response.code == 200 ? response.data.total : 0,
        });
      }
    },

    *fetchInfo({ payload }, { call, put }) {

      const response = yield call(getInfo, payload);
      if(response){
        yield put({
          type: 'setInfo',
          info: response.code == 200 ? response.data : {}
        });
      }
    }

  },
  reducers: {
    modifyTable(state, action) {
      return { ...state, pagination: action.pagination, id:action.id, searchAccount:action.searchAccount, searchName:action.searchName};
    },
    queryList(state, action) {
      return { ...state, list: action.list,  pagination: {...state.pagination, total:action.total} };
    },
    setInfo(state, action) {
      return { ...state,  info: action.info };
    }
  },
};
export default Model;
