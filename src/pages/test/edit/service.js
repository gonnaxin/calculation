import request from 'umi-request';

export async function queryList(params) {
  return request(API_SERVER + 'backend-service/api/exam/info?id='+params.id
  );
}

export async function sendResult(params) {
  return request(API_SERVER + 'backend-service/api/exam/update', {
    method:'POST',
    headers: { 'Content-Type': 'application/json' },
    data:JSON.stringify(params)
  });
}
