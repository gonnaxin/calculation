import { queryFakeList,deleteExam } from './service';

const Model = {
  namespace: 'examList',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total:0
    },
    searchWords:''
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'modifyTable',
        pagination: payload.pagination,
        searchWords:payload.searchWords
      });
      const response = yield call(queryFakeList, payload);

      if(response){
        yield put({
          type: 'finishQueryList',
          payload: {
            list:response.code == 200 ? response.data.list : [],
            total:response.code == 200 ? response.data.total : 0,
          }
        });
      }
    },

    *delete({ payload }, { call, put }) {
      const response = yield call(deleteExam, payload);


      if(response){
        yield put({
          type: 'fetch',
          payload: payload
        });
      }
    },
  },
  reducers: {
    modifyTable(state, action) {
      return { ...state, pagination: action.pagination, searchWords:action.searchWords };
    },
    finishQueryList(state, action) {
      return { ...state, list:action.payload.list, pagination: {...state.pagination, total:action.payload.total}};
    },
  },
};
export default Model;
