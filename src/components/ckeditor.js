import React, { Component } from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import EssentialsPlugin from '@ckeditor/ckeditor5-essentials/src/essentials';
import BoldPlugin from '@ckeditor/ckeditor5-basic-styles/src/bold';
import ItalicPlugin from '@ckeditor/ckeditor5-basic-styles/src/italic';
import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';
import Image from '@ckeditor/ckeditor5-image/src/image';
import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption';
import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle';
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload';

class Editor extends Component {
  constructor() {
    super();
    this.state ={
      data: "<p>Hello from CKEditor 5!</p>"
    }
  }

  render() {
    return (
      <div className="editor">
        <CKEditor
          editor={ ClassicEditor }
          data={this.state.data}
          config={ {
            plugins: [
              EssentialsPlugin, Paragraph,
              BoldPlugin,
              ItalicPlugin,
              SimpleUploadAdapter,
              Image,
              ImageCaption,
              ImageStyle,
              ImageToolbar,
              ImageUpload,
            ],
            toolbar: [
              'bold',
              'italic',
              'uploadImage'
            ],
            simpleUpload: {
              // The URL that the images are uploaded to.
              uploadUrl: 'http://example.com',

              // Enable the XMLHttpRequest.withCredentials property.
              withCredentials: true,

              // Headers sent along with the XMLHttpRequest to the upload server.
              headers: {
                'X-CSRF-TOKEN': 'CSRF-Token',
                Authorization: 'Bearer <JSON Web Token>'
              }
            }
          } }
          onReady={ editor => {
            // You can store the "editor" and use when it is needed.
            console.log( 'Editor is ready to use!', editor );
          } }
          onChange={ ( event, editor ) => {
            const data = editor.getData();
            this.setState({data})
            // console.log(this.state.data)
            // console.log( { event, editor, data } );
          } }
          onBlur={ ( event, editor ) => {
            // console.log( 'Blur.', editor );
          } }
          onFocus={ ( event, editor ) => {
            // console.log( 'Focus.', editor );
          } }
        />
      </div>
    );
  }
}

export default Editor;
