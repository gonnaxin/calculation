import { PlusOutlined } from '@ant-design/icons';
import {Button, Card, Input, List, Space, Table, Tabs, Typography, Modal, Dropdown, Menu, Form, Select, message} from 'antd';
import React, { Component } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
const { Paragraph } = Typography;
import { history } from 'umi';
import moment from 'moment';
import { DownOutlined } from '@ant-design/icons';

const { TextArea } = Input;

const statusMap ={
  99:'报名中',
  201:'已付款',
  202:'已退款',
  203:'已激活'
}

class SignStudentList extends Component {
  formRef = React.createRef();

  constructor() {
    super();
    this.state= {
      isModalVisible: false,
      id: -1
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
    this.setState({
      id: this.props.match.params.id
    },function(){
        dispatch({
          type: 'signStudentList/fetch',
          payload: {
            pagination:{
              current: this.props.pagination.current,
              pageSize: this.props.pagination.pageSize,
              total: this.props.pagination.total,
            },
            id:this.state.id
          },
        });
      }
    )

  }

  add(){
    this.setState({
      isModalVisible:true
    })
  }

  download = async() => {
    try{
      const response = await fetch(API_SERVER +  'backend-service/api/sign/user/csv/download?signUpId='+this.state.id,
        {
          method: 'GET', // *GET, POST, PUT, DELETE, etc.
          // mode: 'cors', // no-cors, *cors, same-origin
          // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'x-auth-token': localStorage.getItem("token"),
          },
          responseType: 'blob'
          // redirect: 'follow', // manual, *follow, error
          // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          // body: JSON.stringify(data) // body data type must match "Content-Type" header
        });


      // 将文件流转为blob对象，并获取本地文件链接
      response.blob().then((blob) => {
        const a = window.document.createElement('a');
        const downUrl = window.URL.createObjectURL(blob);// 获取 blob 本地文件连接 (blob 为纯二进制对象，不能够直接保存到磁盘上)
        const filename = response.headers.get('Content-Disposition').split('filename=')[1].split('.');
        a.href = downUrl;
        a.download = `${decodeURI(filename[0])}.${filename[1]}`;
        a.click();
        window.URL.revokeObjectURL(downUrl);
      });

    }
    catch(err)
    {
      console.log(err)
    }
  }


  handleTableChange = (pagination, filters, sorter,e) => {
    // console.log(pagination,filters, sorter)
    // this.fetch({
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   pagination,
    //   ...filters,
    // });

    //much action may activate table change,only handle paginate part
    if(e.action != 'paginate'){
      return;
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'signStudentList/fetch',
      payload: {
        pagination:{
          current: pagination.current,
          pageSize:  pagination.pageSize,
          total: pagination.total,
        },
        id:this.state.id
      },
    });
  };

  showModal() {
    this.setState({
      isModalVisible:true
    })

  }


  handleOk = async() =>{
    const { dispatch } = this.props;
    // const values = await this.formRef.current.validateFields().then(values=>values, err =>err);
    try{
      const values = await this.formRef.current.validateFields();

      const data = {
        "education": values.education,
        "name": values.name,
        "occupation": values.occupation,
        "passport": ""+values.passport.country+values.passport.number,
        "postAddress": ""+values.address.country+values.address.fullAddress,
        "postCode": values.postCode,
        "signUpId": this.state.id,
        "status": 203
      }

      const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign/user/add',
        {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem("token"),
          },
          // redirect: 'follow', // manual, *follow, error
          // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        }).then(response => response.json()
      )

      if(returnResponse. code === 200){
        this.setState({isModalVisible:false},
          function(){
            dispatch({
              type: 'signStudentList/fetch',
              payload: {
                pagination:{
                  current: this.props.pagination.current,
                  pageSize: this.props.pagination.pageSize,
                  total: this.props.pagination.total,
                },
                id:this.state.id
              },
            });
          }
        )
      }
      else{
        message.error(returnResponse.message)
      }
    }
    catch(err)
    {
      console.log(err)
    }

  }

  handleCancel(){
    this.setState({isModalVisible:false})
  }

  onClick = async( studentId, key ) => {
    const { dispatch } = this.props;
    // const values = await this.formRef.current.validateFields().then(values=>values, err =>err);
    try{

      const data = {
        "studentId": studentId,
        "signUpId": this.state.id,
        "status": key
      }

      const returnResponse = await fetch(API_SERVER + 'backend-service/api/sign/user/update',
        {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          // credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem("token"),
          },
          // redirect: 'follow', // manual, *follow, error
          // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        }).then(response => response.json()
      )

      if(returnResponse. code === 200){
        this.setState({isModalVisible:false},
          function(){
            dispatch({
              type: 'signStudentList/fetch',
              payload: {
                pagination:{
                  current: this.props.pagination.current,
                  pageSize: this.props.pagination.pageSize,
                  total: this.props.pagination.total,
                },
                id:this.state.id
              },
            });
          }
        )
      }
      else{
        message.error(returnResponse.message)
      }
    }
    catch(err)
    {
      console.log(err)
    }
  };



  render() {


    const {
      list,
      loading,
      pagination
    } = this.props;



    const columns = [
      {
        key:1,
        title: '护照号',
        dataIndex: 'passport',
        // sorter: true,
        // sortOrder:'descend'
      },
      {
        key:2,
        title: '姓名',
        dataIndex: 'name'
      },
      {
        key:3,
        title: '邮寄地址',
        dataIndex: 'postAddress'
      },
      {
        key:4,
        title: '邮编',
        dataIndex: 'postCode'
      },
      {
        key:5,
        title: '学历',
        dataIndex: 'education'
      },
      {
        key:6,
        title: '职业',
        dataIndex: 'occupation'
      },

      {
        key:7,
        title: '状态',
        dataIndex: 'operations',
        render: (text, record) => {
            let menuItemMap ;
            let innerMenu =[];
          for(let item in statusMap){
            if(record.status != item){
              innerMenu.push( <Menu.Item key={item}>{statusMap[item]}</Menu.Item>)
            }
          }
            menuItemMap = <Menu onClick={({key}) => this.onClick(record.studentId, key)}>
              {innerMenu}
            </Menu>

          return (
            <Dropdown overlay={
              menuItemMap
            } trigger={['click']}>
              <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                {statusMap[record.status]} <DownOutlined />
              </a>
            </Dropdown>
          )
        }
      },
    ];

    const content = (
      <div className={styles.pageHeaderContent}>

      </div>
    );

    const layout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 19 },
    };

    const nullData = {};
    return (
      <PageContainer content={content} className={styles.containerWrap}>
        <Table
          columns={columns}
          rowKey={record => record.studentId}
          dataSource={list}
          pagination={pagination}
          loading={loading}
          onChange={this.handleTableChange}
        />
        <div className={styles.buttonArea}>
          <Button type="primary"  className={styles.buttonLeft} onClick={()=>this.add()}>+添加考生</Button>
          <Button type="primary"  className={styles.buttonRight} onClick={()=>this.download()}>下载CSV</Button>
        </div>
        <Modal title="编辑用户信息" className={styles.editModal} width={596}  okText={"添加"} cancelText={"取消"} visible={this.state.isModalVisible} onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)} closable={false} maskClosable={false} destroyOnClose={true} >
          <div className={styles.formArea}>
            <Form
              {...layout}
              name="normal_login"
              className="login-form"
              initialValues={{ remember: true }}
              ref={this.formRef}
            >
              <Form.Item
                label="Name:"
                name="name"
                rules={[{ required: true, message: 'Please input your Name!' }]}
              >
                <Input
                  bordered={true}
                  // placeholder="username"
                />
              </Form.Item>
              <Form.Item
                label="Passport"
                className={styles.passportLabel}
              >
                <Input.Group compact>
                  <Form.Item
                    name={['passport', 'country']}
                    noStyle
                    rules={[{ required: true, message: 'Country is required' }]}
                  >
                    <Select placeholder="Country" style={{ width: '30%' }}>
                      <Select.Option value="(86)">中国</Select.Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name={['passport', 'number']}
                    noStyle
                    rules={[{ required: true, message: 'passport number is required' }]}
                  >
                    <Input
                      bordered={true}
                      style={{ width: '70%' }}
                      // placeholder="username"
                    />
                  </Form.Item>
                </Input.Group>
              </Form.Item>

              <Form.Item
                label="Post Address"
                className={styles.addressLabel}
              >
                <Input.Group compact>
                  <Form.Item
                    name={['address', 'country']}
                    noStyle
                    rules={[{ required: true, message: 'Country is required' }]}
                  >
                    <Select placeholder="Country" style={{ width: '100%' }}>
                      <Select.Option value="(86)">中国</Select.Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name={['address', 'fullAddress']}
                    noStyle
                    rules={[{ required: true, message: 'address is required' }]}
                  >
                    <TextArea rows={4} style={{ width: '100%' , marginTop:"16px"}} placeholder="Input full address" />
                  </Form.Item>
                </Input.Group>
              </Form.Item>

              <Form.Item
                label="Postcode"
                name="postCode"
                rules={[{ required: true, message: 'Please input your post code!' }]}
              >
                <Input
                  bordered={true}
                  // placeholder="Password"
                />
              </Form.Item>

              <Form.Item
                label="Education"
                name="education"
              >
                  <Select>
                    <Select.Option value="1">小学</Select.Option>
                    <Select.Option value="2">初中</Select.Option>
                    <Select.Option value="3">高中</Select.Option>
                    <Select.Option value="4">本科</Select.Option>
                    <Select.Option value="5">研究生</Select.Option>
                    <Select.Option value="6">博士生</Select.Option>
                </Select>
              </Form.Item>

              <Form.Item
                label="Occupation"
                name="occupation"
              >
                <Input
                  bordered={true}
                  // placeholder="Password"
                />
              </Form.Item>

            </Form>
          </div>
        </Modal>
      </PageContainer>
    );
  }
}

export default connect(({ signStudentList, loading }) => ({
  list: signStudentList.list,
  pagination: signStudentList.pagination,
  loading: loading.models.signStudentList,
}))(SignStudentList);
