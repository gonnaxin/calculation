import { queryList, sendResult} from './service';
import { history } from 'umi';
import { message} from 'antd';

const pick = (obj, arr) =>
  arr.reduce((iter, val) => (val in obj && (iter[val] = obj[val]), iter), {});

const initValue = {
  id:0,
  title:'',
  registerStartTime:'',
  registerEndTime:'',
  startTime:'',
  endTime:'',
  description:'',
  paper: {id:-1},
  signUpType:0,
  examineeCsvUrl:[],
  invigilateType:0,
  type:0,
  processCfg:{},
  subject:"",
  paperLink:""
}

const keyMap = ['id', 'title', 'registerStartTime','registerEndTime','startTime','endTime','description','paper','signUpType','examineeCsvUrl','invigilateType','type','processCfg','subject','paperLink'];

const Model = {
  namespace: 'testEdit',
  state: initValue,
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryList, payload);
      yield put({
        type: 'init',
        payload: response.status == 'ok' ? response.data : initValue
      });
    },
    *submitResult({payload}, { call, put }) {
      let cachePayload = JSON.parse(JSON.stringify(payload));
      if(cachePayload.examineeCsvUrl.length >0){
        cachePayload.examineeCsvUrl = cachePayload.examineeCsvUrl[0].url
      }
      else{
        cachePayload.examineeCsvUrl = "";
      }
      if(cachePayload.paper.id === -1 ){
        cachePayload.paper = null;
      }
      const response = yield call(sendResult, cachePayload);
      if(response.status === "ok"){
        history.push('/test');
      }
      else{
          message.error(response.message)
      }


    }
  },
  reducers: {
    init(state, action){
      // const {...obj} =  action.payload;
      let obj = pick(action.payload, keyMap);
      obj.examineeCsvUrl = obj.examineeCsvUrl!==""? [{uid:'-1',name: obj.examineeCsvUrl, status:'done', url: obj.examineeCsvUrl}]:[];
      obj.paper = action.payload.paper?obj.paper:{id:-1};
      return obj;
    },
    modifyState(state, action){
      let stateObj = JSON.parse(JSON.stringify(state));
      stateObj[action.payload.key] = action.payload.value;
      return stateObj;
    },
    modifyTimeState(state, action){
      let stateObj = JSON.parse(JSON.stringify(state));
      switch(action.payload.key){
        case 'register':
          stateObj.registerStartTime= action.payload.value[0];
          stateObj.registerEndTime= action.payload.value[1];
          break;
        case 'test':
          stateObj.startTime= action.payload.value[0];
          stateObj.endTime= action.payload.value[1];
          break;
      }

      return stateObj;
    }
  },
};
export default Model;
