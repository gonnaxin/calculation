import { PlusOutlined } from '@ant-design/icons';
import React, { Component  } from 'react';
import {Row, Col, Divider, List, Checkbox} from 'antd';
import { Tabs , Input, Card ,Button , Modal,Select,Spin, Popover,DatePicker,Upload,message } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
import moment from 'moment';
import 'moment/locale/zh-cn';
import locale from 'antd/lib/locale/zh_CN';
import { UploadOutlined } from '@ant-design/icons';
import debounce from 'lodash/debounce';
import { history } from 'umi';
import StudentList from "./components/studentList";
import ChartsPage from "./components/charts";
import StudentVideo from "./components/studentVideo";


const { RangePicker } = DatePicker;
const { TabPane } = Tabs;
const invigilateTypeMap = {0:"不监考", 1:"拍照",2:"直播"}

class TestMain extends Component {
  constructor() {
    super();
    this.state= {
      paperList:[],
      fetching: false,
    }
    this.edit = this.edit.bind(this);
    this.delete = this.delete.bind(this);
    this.fetchPapers = debounce(this.fetchPapers, 800);
    this.lastFetchId = 0;
  }
  componentDidMount() {
    const { dispatch } = this.props;
    const params =this.props.match.params.id;
    dispatch({
      type: 'testMain/fetch',
      payload: {
        id:params
      }
    });
   // this.fetchPapers('')
  }

  edit(){
    history.push(`/test/edit/`+this.props.match.params.id);
  }
  delete(){
    const { dispatch } = this.props;
    const params =this.props.match.params.id;
    dispatch({
      type: 'testMain/delete',
      payload: {
        id:params
      }
    });
    message.success('删除成功');
  }

  fetchPapers(params){
    this.lastFetchId += 1;
    const fetchId = this.lastFetchId;
    this.setState({ data: [], fetching: true });
    fetch(API_SERVER + 'backend-service/api/paper/list?page=1&size=100&keyword='+params,
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
      .then(response => response.json())
      .then(body => {
        if (fetchId !== this.lastFetchId) {
          // for fetch callback order
          return;
        }

        if(body.status =='ok'){
          this.setState({paperList:body.data.list});
        }
        this.setState({ fetching: false });
      });
  }



  render() {

    const {
      testMain ,
      loading,
    } = this.props;

   const earlytime = (this.props.testMain.processCfg.tasks && this.props.testMain.processCfg.tasks[0].checked) ? this.props.testMain.registerStartTime : this.props.testMain.startTime;
   const disableFlag = earlytime <= new Date().getTime();

    const content = (
      <div className={styles.pageHeaderContent}>
        <span className={styles.title}>{this.props.testMain.title}</span>
        <Button type="primary" disabled={false} className={styles.buttonGroups} onClick={this.edit}>编辑考试</Button>
        <Button type="danger" disabled={false} style={{marginRight: '24px'}} className={styles.buttonGroups} onClick={this.delete}>删除</Button>
      </div>
    );

    const processArray =[];
    if(this.props.testMain.processCfg.tasks){
      for(let i=0; i< this.props.testMain.processCfg.tasks.length;i++){
        if(this.props.testMain.processCfg.tasks[i].checked){
          processArray.push(this.props.testMain.processCfg.tasks[i])
        }
      }
    }


    return (
      <Spin spinning={loading?loading:false}>
        <PageContainer content={content} >
          <Tabs defaultActiveKey="1" >

            <TabPane tab="考试信息" key="1">
              <Row className={styles.testContainer}  >
                <Divider orientation="left">{examSetting['/test/create']['divider1']}</Divider>
                <Col span={24} className={styles.formArea} style={{'textAlign':'left', marginLeft:"36px"}}>
                  <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'0px'}} >
                    <Col >{examSetting['/test/create']['area1-8']}</Col>
                    <Col span={16} >{this.props.testMain.subject}</Col>
                  </Row>

                  <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}}>
                    <Col >{examSetting['/test/create']['area1-3']}</Col>
                    <Col span={16} >
                      {this.props.testMain.startTime == ''?null: moment(this.props.testMain.startTime).format('YYYY-MM-DD HH:mm:ss')}
                      {'\u00A0\u00A0-\u00A0\u00A0'}
                      {this.props.testMain.endTime == ''?null: moment(this.props.testMain.endTime).format('YYYY-MM-DD HH:mm:ss')}
                    </Col>
                  </Row>
                  {/*<Row gutter={[8,16]}>  <Col span={4} className={styles.formTitle}>辅助功能:</Col>*/}
                  {/*  <Col span={16} style={{'textAlign':'left','marginTop':'4px'}}>无</Col>*/}
                  {/*</Row>*/}

                </Col>
                <Col span={24} className={styles.formArea}  style={{'textAlign':'left', marginLeft:"36px"}}>
                  {this.props.testMain.paperLink == ""?
                    <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}}>
                    <Col> {examSetting['/test/create']['area1-5']}</Col>
                    <Col span={16} >{this.props.testMain.paper.id === -1?"":this.props.testMain.paper.title}</Col>
                  </Row>:""
                  }
                  {this.props.testMain.paperLink !== ""?
                    <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}}>
                    <Col >{examSetting['/test/create']['area1-9']}</Col>
                    <Col span={16} >{this.props.testMain.paperLink}</Col>
                  </Row>:""
                  }
                  <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}}>
                    <Col >{examSetting['/test/create']['area1-6']}</Col>
                    <Col span={16} >线下报名</Col>
                  </Row>
                  <Row gutter={[8,16]} style={{'textAlign':'left','marginTop':'16px'}}>
                    <Col >{examSetting['/test/create']['area1-7']}</Col>
                    <Col span={16} >{this.props.testMain.examineeCsvUrl.length > 0?this.props.testMain.examineeCsvUrl[0].name:"" }</Col>
                  </Row>
                  <Row gutter={[8,16]} style={{ 'textAlign':"left",'marginTop':'16px'}}>
                    <Col  >{examSetting['/test/create']['area1-4']}</Col>
                    <Col span={16} >
                      {this.props.testMain.description}
                    </Col>
                  </Row>
                </Col>

                <Divider orientation="left" style={{ marginTop:"64px"}}>{examSetting['/test/create']['divider2']}</Divider>
                <Col span={24} className={styles.formArea}>
                  <Col span={24} className={styles.formArea} style={{'textAlign':'left','marginLeft':'36px'}}>
                    {
                      processArray.map((item,index) => {
                        const beforeInfo = <div className={styles.configAreaBorder}>
                          <Row style={{margin:"0px 24px 4px 24px"}} span={24}> <Col  className={styles.formTitle}>{examSetting['/test/create']['area1-2']}</Col>
                            <Col span={18} style={{margin:"4px 0px 0px 16px"}}>
                              {
                                (this.props.testMain.registerStartTime === ''?"": moment(this.props.testMain.registerStartTime).format("YYYY-MM-DD HH:mm:ss")) + ' - ' +
                                (this.props.testMain.registerEndTime === ''?"":moment(this.props.testMain.registerEndTime).format("YYYY-MM-DD HH:mm:ss"))
                              }
                            </Col>
                          </Row>
                        </div>;
                        const testInfo =<div className={styles.configAreaBorder}>
                          <Row style={{margin:"0px 24px 4px 24px"}} span={24}> <Col   className={styles.formTitle}>{examSetting['/test/create']['area1-3']}</Col>
                            <Col span={18} style={{margin:"4px 0px 0px 16px"}}>
                              {
                                (this.props.testMain.startTime === ''?"": moment(this.props.testMain.startTime).format("YYYY-MM-DD HH:mm:ss")) + ' - ' +
                                (this.props.testMain.endTime === ''?"":moment(this.props.testMain.endTime).format("YYYY-MM-DD HH:mm:ss"))
                              }
                            </Col>
                            <Col span={24}>
                              考试结束前 {this.props.testMain.remindBeforeEnd}
                              秒，提醒考生
                            </Col>
                          </Row>
                        </div>;

                        if(item.taskName === '考前注册'){
                          return <div  key={index}> {(index +1) +". "+ item.taskName} {beforeInfo}</div>
                        }
                        if (item.taskName === '考试'){
                          return <div  key={index}> {(index +1) +". "+ item.taskName}  {testInfo}</div>

                        }
                        return  <div  key={index}> {(index +1) +". "+ item.taskName} </div>
                      })
                    }
                  </Col>
                </Col>

                <Divider orientation="left" style={{ marginTop:"64px"}}>{examSetting['/test/create']['divider3']}</Divider>
                <Col span={12} className={styles.formArea}  style={{'textAlign':'left','marginLeft':'36px'}}>
                  <Row gutter={[8,16]}>
                    <Col >{examSetting['/test/create']['area2-1']}</Col>
                    <Col span={16} >{invigilateTypeMap[this.props.testMain.invigilateType]}</Col>
                    {(this.props.testMain.invigilateType ===1 && testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[0].isChecked) ?<Col span={24} >图像识别</Col>:""}
                    {(this.props.testMain.invigilateType ===1 && testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[0].events[0].isChecked) ?<Col span={20} style={{'marginTop':"-12px",'marginLeft':'28px'}}>检测到无法识别面部记录</Col>:""}
                    {(this.props.testMain.invigilateType ===1 && testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[0].events[1].isChecked) ?<Col span={20} style={{'marginTop':"-12px",'marginLeft':'28px'}}>检测到识别到多个面部记录</Col>:""}
                    {(this.props.testMain.invigilateType ===1 && testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[0].events[2].isChecked) ?<Col span={20} style={{'marginTop':"-12px",'marginLeft':'28px'}}>检测到出现非本人面部记录</Col>:""}
                    {(this.props.testMain.invigilateType ===1 && testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[0].events[3].isChecked) ?<Col span={20} style={{'marginTop':"-12px",'marginLeft':'28px'}}>检测到使用手机记录</Col>:""}


                    {(testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[1].isChecked) ?<Col span={24} >智能检测</Col>:""}
                    {(testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[1].events[2].isChecked) ?<Col span={20} style={{'marginTop':"-12px",'marginLeft':'28px'}}>霸屏考试</Col>:""}
                    {(testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[1].events[3].isChecked) ?<Col span={20} style={{'marginTop':"-12px",'marginLeft':'28px'}}>禁止特殊操作</Col>:""}
                    {(testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[1].events[0].isChecked) ?<Col span={20} style={{'marginTop':"-12px",'marginLeft':'28px'}}>
                        考生切屏出登录
                    </Col>:""}
                    {(testMain.monitorConfig.monitorCategories && testMain.monitorConfig.monitorCategories[1].events[1].isChecked) ?<Col span={20} style={{'marginTop':"-12px",'marginLeft':'28px'}}>
                    本次考试内考生登录次数达到{testMain.monitorConfig.monitorCategories[1].events[1].limit}次时，强制结束考试
                    </Col>:""}
                  </Row>
                </Col>

              </Row>
            </TabPane>
            <TabPane tab="考生列表" key="2">
              <StudentList examId={this.props.match.params.id} invigilateType={this.props.testMain.invigilateType}></StudentList>
            </TabPane>
            <TabPane tab="考试分析" key="3">

            </TabPane>
            <TabPane tab="监考情况" key="4">
              <ChartsPage examId={this.props.match.params.id}  examStatus={this.props.location.query.examStatus}></ChartsPage>
            </TabPane>
            {
              this.props.testMain.invigilateType === 1?   <TabPane tab="直播情况" key="5" >
              <StudentVideo examId={this.props.match.params.id} invigilateType={this.props.testMain.invigilateType}></StudentVideo>
                </TabPane>:""
            }
          </Tabs>
        </PageContainer>
      </Spin>
    );
  }
}

export default connect(({ testMain, loading }) => ({
  testMain,
  loading: loading.models.testMain,
}))(TestMain);
