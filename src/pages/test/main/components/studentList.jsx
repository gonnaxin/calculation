import React, {useEffect, useMemo, useRef, useState} from "react";
import {connect, history} from "umi";
import {Space, Table} from "antd";

const StudentList = (props) => {
  const {examId}= props;

  const [pagination,setPage] = useState({
    current: 1,
    pageSize: 100,
    total:0
  });
  const [studentList, setStudentList] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    fetchPapers(examId,pagination)
  }, []);

  const handleTableChange = (pagination, filters, sorter,e) => {
    setPage(pagination);
    fetchPapers(examId, pagination);
  }

  const jump=(record)=>{
      history.push(`/test/studentInfo/${record.userId}/${examId}/${record.familyName+record.givenName}`);
  }

  const fetchPapers = (examId, pagination) => {
    setLoading(true);
    fetch(API_SERVER + `backend-service/api/exam/user/list?page=${pagination.current}&size=${pagination.pageSize}&examId=`+examId,
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
      .then(response => response.json())
      .then(body => {

        if(body.status =='ok'){
          setStudentList(body.data.list);
          pagination.total = body.data.total;
          setPage(pagination);
        }
        setLoading(false)
      });
  }

  const columns = [
    {
      key:1,
      title: '学号',
      dataIndex: 'studentId',
      // sorter: true,
      // sortOrder:'descend',
    },
    {
      key:2,
      title: '姓名',
      dataIndex: 'familyName',
      render: (text, record) => {
        return (
          <div > {record.familyName + record.givenName}</div>
        )
      }
    },
    {
      key:3,
      title: '邮箱',
      dataIndex: 'email'
    },
    {
      key:4,
      title: '成绩',
      dataIndex: 'score'
    },
    {
      key:5,
      title: '本人度',
      dataIndex: 'match',
      render: (text, record) => {
        return (
          <div> {(record.match*100).toFixed(2)+ '%'}</div>
        )
      }
    },
    {
      key:6,
      title: '照片记录',
      dataIndex: 'totalPhoto',
      render: (text, record) => {
        return (
          <div> {`${record.validPhoto }/${ record.totalPhoto}`}</div>
        )
      }
    },
    {
      key:7,
      title: '状态',
      dataIndex: 'cheating',
      render: (text, record) => {
        return (
          <div> {record.cheating?'作弊':'正常' }</div>
        )
      }
    },
    {
      key:8,
      title: '操作',
      dataIndex: 'operation',
      render: (text, record) => {
        return (
          <Space size="middle">
            <a onClick={() => jump(record)}>查看详情</a>
          </Space>
        )
      }
    },
  ];

  return (
    <Table
      columns={columns}
      rowKey={record => record.studentId}
      dataSource={studentList}
      pagination={pagination}
      loading={loading}
      onChange={handleTableChange}
    />
  );
};

export default StudentList;
