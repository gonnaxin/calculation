import { queryList, deleteExam} from './service';
import { history } from 'umi';


const pick = (obj, arr) =>
  arr.reduce((iter, val) => (val in obj && (iter[val] = obj[val]), iter), {});

const initValue = {
  id:0,
  title:'',
  registerStartTime:'',
  registerEndTime:'',
  remindBeforeEnd:0,
  startTime:'',
  endTime:'',
  description:'',
  paper: {id:-1},
  signUpType:0,
  examineeCsvUrl:[],
  invigilateType:0,
  type:0,
  processCfg:{},
  monitorConfig:{},
  subject:"",
  paperLink:""
}


const keyMap = ['id', 'title', 'registerStartTime','registerEndTime','startTime','endTime','description','paper','signUpType','examineeCsvUrl','invigilateType','type','processCfg','monitorConfig', 'subject','paperLink','remindBeforeEnd'];

const Model = {
  namespace: 'testMain',
  state: initValue,
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryList, payload);
      yield put({
        type: 'init',
        payload: response.status == 'ok' ? response.data : initValue
      });
    },
    *delete({payload}, { call, put }) {
      const response = yield call(deleteExam, payload);
      if(response){
        history.push('/test');
        // yield put({
        //   type: 'submitFinish',
        //   result: response
        // })
      }
    }
  },
  reducers: {
    init(state, action){
      // const {...obj} =  action.payload;
      let obj = pick(action.payload, keyMap);
      obj.examineeCsvUrl = [{uid:'-1',name: obj.examineeCsvUrl, status:'done', url: obj.examineeCsvUrl}];
      obj.paper = action.payload.paper?obj.paper:{id:-1};
      return obj;
    },
    modifyState(state, action){
      let stateObj = JSON.parse(JSON.stringify(state));
      stateObj[action.payload.key] = action.payload.value;
      return stateObj;
    },
    modifyTimeState(state, action){
      let stateObj = JSON.parse(JSON.stringify(state));
      switch(action.payload.key){
        case 'register':
          stateObj.registerStartTime= action.payload.value[0];
          stateObj.registerEndTime= action.payload.value[1];
          break;
        case 'test':
          stateObj.startTime= action.payload.value[0];
          stateObj.endTime= action.payload.value[1];
          break;
      }

      return stateObj;
    }
  },
};
export default Model;
