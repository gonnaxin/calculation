import { queryUserAllPhotos, queryUserBigPhoto,queryUserIncidents ,updateCheating, updateScore } from './service';
import {message} from 'antd'

const Model = {
  namespace: 'StudentInfo',
  state: {
    info: {},
    bigPhoto:{},
    incidents:{}
  },
  effects: {
    *fetch({ payload }, { call, put, all }) {
      const [userPhotos, bigPhoto, incidents]  = yield all([
        call(queryUserAllPhotos, payload),
        call(queryUserBigPhoto, payload),
        call(queryUserIncidents, payload),
      ])

      if(userPhotos){
        yield put({
          type: 'getData',
          payload:  userPhotos,
        });
      }

      if(bigPhoto){
        yield put({
          type: 'getBigPhoto',
          payload:  bigPhoto.data,
        });
      }

      if(incidents){
        yield put({
          type: 'getIncidents',
          payload:  incidents,
        });
      }


    },

    *updateCheating({payload},{call, put}){

      const result  = yield call(updateCheating, payload);
      if(result.status === 'ok'){
        const bigPhoto  = yield call(queryUserBigPhoto, payload);

        if(bigPhoto){
          yield put({
            type: 'getBigPhoto',
            payload:  bigPhoto.data,
          });
        }
      }
      else{
        message.error(result.message);
      }

    },

    *updateScore({payload},{call, put}){

      const result  = yield call(updateScore, payload);
      if(result.status === 'ok'){
        const bigPhoto  = yield call(queryUserBigPhoto, payload);

        if(bigPhoto){
          yield put({
            type: 'getBigPhoto',
            payload:  bigPhoto.data,
          });
        }
      }
      else{
        message.error(result.message);
      }

    }

  },
  reducers: {
    getData(state, action) {
      return { ...state, info: action.payload };
    },
    getBigPhoto(state, action) {
      return { ...state, bigPhoto: action.payload };
    },
    getIncidents(state, action) {
      return { ...state, incidents: action.payload };
    },
  },
};
export default Model;
