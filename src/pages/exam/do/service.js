import request from 'umi-request';

export async function queryFakeList(params) {
  return request(API_SERVER + 'backend-service/api/paper/info?id='+params.id
  );
}


export async function sendResult(params) {
  return request(API_SERVER + 'backend-service/api/exam/paper/submit', {
    method:'POST',
    headers: { 'Content-Type': 'application/json' },
    data:JSON.stringify(params)
  });
}
