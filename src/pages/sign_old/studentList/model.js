import { queryFakeList } from './service';

const Model = {
  namespace: 'signStudentList_old',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 5,
      total:0
    },
    id:-1
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'modifyTable',
        pagination: payload.pagination,
        id:payload.id
      });

      const response = yield call(queryFakeList, payload);
      if(response){
        yield put({
          type: 'queryList',
          list: response.code == 200 ? response.data.list : [],
          total:response.code == 200 ? response.data.total : 0,
        });
      }
    }

  },
  reducers: {
    modifyTable(state, action) {
      return { ...state, pagination: action.pagination, id:action.id };
    },
    queryList(state, action) {
      return { ...state, list: action.list,  pagination: {...state.pagination, total:action.total} };
    }
  },
};
export default Model;
