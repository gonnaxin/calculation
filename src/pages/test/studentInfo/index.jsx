import { PlusOutlined } from '@ant-design/icons';
import {Button, Card, Input, List, Tabs, Typography, Row, Col, Spin, Space, Table, Modal, message} from 'antd';
import React, { Component } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
const { Paragraph } = Typography;
import { history } from 'umi';
import moment from 'moment';

const { TabPane } = Tabs;

function callback(key) {

}

const EventType = {
  '0': '身份验证失败',
  '1': '没有检测到人脸',
  '2': '多个人脸',
  '3': '检测到手机',
  '100': '切换屏幕',
  '101': '重新登入',
  '102': '屏幕大小变化',
  '103': '复制粘贴等操作',
}

const CategoryType = {
  '0': '正面照片',
  '1': '浏览器行为',
}

class StudentInfo extends Component {
  constructor(props) {
    super(props);
    this.state={
      bigPhoto:null,
      isModalVisible:false,
      mainInfo:"",
      scoreModalVisible:false,
      quizId:-1,
      quizScore:0
    };
  }


  componentDidMount() {
    const { dispatch } = this.props;
    const id =this.props.match.params.id;
    const examId =this.props.match.params.examId;
    dispatch({
      type: 'StudentInfo/fetch',
      payload: {
        id: id,
        examId:examId
      },
    });
  }

  changePhoto(index){

    let photos =  document.getElementsByClassName("photoGroups");
    for(let i=0;i<photos.length;i++){
      photos[i].classList.remove('selected');
    }
    document.getElementsByClassName("photoGroups")[index].classList.add('selected');

    this.setState({
      bigPhoto: this.props.StudentInfo.info['monitor_photos'][index]
    });
  }

  checkDetail(path){
    this.setState({
      mainInfo:path,
      isModalVisible:true
    })
  }

  updateCheating= () => {
    const { dispatch } = this.props;
    const {id} = this.props.match.params;
    const {examId} = this.props.match.params;

    dispatch({
      type: 'StudentInfo/updateCheating',
      payload: {
        cheating: !this.props.StudentInfo.bigPhoto.status,
        id,
        examId
      },
    });
  }

  updateScore= (score, quizId) => {
    if(score && !/^(([1-9]\d{1,3}(\.\d{1,2})?)|\d((\.\d{1,2})?)|10000|10000.0|10000.00)$/.test(score)){
      message.error("分值必须是0-10000的整数或两位小数");
      return
    }

    const { dispatch } = this.props;
    const {id} = this.props.match.params;
    const {examId} = this.props.match.params;

    dispatch({
      type: 'StudentInfo/updateScore',
      payload: {
        score,
        id,
        examId,
        quizId
      }
    });
    this.setState({
      scoreModalVisible : false
    })
  }

  openScoreModal(quizScore,quizId){
    this.setState({
      quizScore,
      quizId,
      scoreModalVisible : true
    });
  }

  modifyQuizScore(e){
    this.setState({
      quizScore:e.target.value
    })
  }

  render() {
    const {
      loading
    } = this.props;

    const StudentInfo = this.props.StudentInfo.info;
    const bigInfo = this.props.StudentInfo.bigPhoto;
    const incidents = this.props.StudentInfo.incidents;

    const content = (
      <div className={styles.pageHeaderContent}>
        <span className={styles.title}>{this.props.match.params.name}</span>
        <Button type="primary"  className={styles.buttonGroups} >下载照片</Button>
        <Button type="primary" style={{marginRight: '24px'}} className={styles.buttonGroups} >答案PDF</Button>
      </div>
    );

    let photos=[];
    for (let index in StudentInfo['monitor_photos']){

      photos.push(<Col span={4} className={styles.photoArea + " photoGroups " +(StudentInfo['monitor_photos'][index].is_invalid?styles.cheatPic:"")} key={index} onClick={this.changePhoto.bind(this,index)}>
        <img src={StudentInfo['monitor_photos'][index].path} style={{width:"100%"}} />

      </Col>)
    }

    const bigInfoColumns = [
      {
        key:1,
        title: '题号',
        dataIndex: 'sort',
        // sorter: true,
        // sortOrder:'descend',
        width: '20%',
      },
      {
        key:2,
        title: '正确答案',
        dataIndex: 'correctAnswer',
        width: '15%',
      },
      {
        key:3,
        title: '考生答案',
        dataIndex: 'event',
        width: '15%',
        render: (text, record) => {
          return (
           <div>{record.isAnswered?record.userAnswer :""}</div>
          )
        }
      },
      {
        key:4,
        title: '得分',
        dataIndex: 'operations',
        width: '20%',
        render: (text, record) => {
          return (
            <Space size="middle">
              {record.score}

              <span style={{marginLeft:'40px',color:'rgba(65, 158, 255, 1)',cursor:'pointer'}}  onClick={()=> this.openScoreModal(record.score, record.quizId)}>修改</span>
            </Space>
          )
        }
      },
    ];

    const columns = [
      {
        key:1,
        title: '时间',
        dataIndex: 'time',
        // sorter: true,
        // sortOrder:'descend',
        width: '20%',
        render: (text, record) => {
          return (
            moment(record.time).format('YYYY-MM-DD HH:mm')
          )
        }
      },
      {
        key:2,
        title: '分类',
        dataIndex: 'category',
        width: '15%',
        render: (text, record) => {
          return (
            CategoryType[`${record.category}`]
          )
        }
      },
      {
        key:3,
        title: '事件',
        dataIndex: 'event',
        width: '15%',
        render: (text, record) => {
          return (
            EventType[`${record.event}`]
          )
        }
      },
      {
        key:4,
        title: '操作',
        dataIndex: 'operations',
        width: '20%',
        render: (text, record) => {
          return (
            <Space size="middle">
              {record.photo_url?<a onClick={() =>(this.checkDetail(record.photo_url))}>查看</a>:""}
            </Space>
          )
        }
      },
    ];


    return (
      <Spin spinning={loading?loading:false}>
      <PageContainer  className={styles.containerWrap}>
        <Row>
          <Col className={styles.leftArea} span={6}>
            <div className={styles.bigPhotoArea}>
              <img src={bigInfo?bigInfo.registerPhotoUrl:""} style={{width:'100%'}}/>
              <div className={styles.personName}>{this.props.match.params.name}</div>
              <div className={styles.personInfo}>{'考试 ID: '+ bigInfo.examId }</div>
              <div className={styles.personInfo}>{'考试名称: '+ bigInfo.examName }</div>
              <div className={styles.personInfo}>{'考试时间: '+ moment(bigInfo.examStartTime).format('YYYY-MM-DD HH:mm') + '-' +moment(bigInfo.examEndTime ).format('HH:mm')}</div>
              <div className={styles.personInfo}>{'考生ID: '+ bigInfo.userId }</div>
              <div className={styles.personInfo}>{'操作时间: '+ moment(bigInfo.examStartTime).format('YYYY-MM-DD HH:mm') + '-' +moment(bigInfo.endTime).format('HH:mm')}</div>
              <div className={styles.personInfo}>{'状态: '+ (bigInfo.status?'作弊':'正常' )}
                {!bigInfo.status?<span style={{marginLeft:'12px',color:'rgba(255, 116, 96, 1)',cursor:'pointer'}}  onClick={this.updateCheating}>标记为作弊</span>
                 : <span style={{marginLeft:'12px',color:'rgba(255, 116, 96, 1)',cursor:'pointer'}}  onClick={this.updateCheating}>标记为正常</span>
                }
              </div>
            </div>
          </Col>
          <Col className={styles.rightArea} span={17} offset={1}>
            {
              (Object.keys(StudentInfo).length > 0 ) ?
                <Tabs defaultActiveKey="1" >
                  <TabPane tab="考试成绩" key="1">
                    <div className={styles.incidentsWord}>
                      考生得分: {bigInfo.score} / {bigInfo.totalScore}
                    </div>
                    <Table
                      columns={bigInfoColumns}
                      rowKey={record => record.sort}
                      dataSource={bigInfo.quizzes}
                      scroll={{y:'400px'}}
                      pagination={{
                        pageSize:1000,
                        hideOnSinglePage:true
                      }}
                    />
                  </TabPane>

                  <TabPane tab="事件记录" key="2">
                    <div className={styles.incidentsWord}>本场考试中可疑行为有{incidents.incident_list? incidents.incident_list.length: 0}次</div>
                    <Table
                      columns={columns}
                      rowKey={record => record.time}
                      dataSource={incidents.incident_list}
                    />

                  </TabPane>


                  <TabPane tab="考试表现" key="3">
                    <div className={styles.incidentsWord}>
                      本场考试拍摄照片{StudentInfo.total_photo}张，其中正常{StudentInfo.total_photo -StudentInfo.unvalid_photo }张，疑似作弊{StudentInfo.unvalid_photo}张
                    </div>
                    <Row>
                      <Col span={18} >
                        <Row>
                          <Col span={24} style={{paddingRight:"28px"}}>
                            <div className={styles.bigPhoto}>
                              {this.state.bigPhoto?
                                <img src={this.state.bigPhoto.path} style={{width:'100%','marginBottom':'8px'}}/>
                              :<div style={{width:"100%",height:"325px",background:"#eee"}}></div>
                              }
                              <div style={{'margin':'8px 8px'}}>记录时间: {this.state.bigPhoto?moment(this.state.bigPhoto['create_time']).format('YYYY-MM-DD HH:mm:ss'):""}</div>
                            </div>
                          </Col>
                          <Col span={24} style={{paddingRight:"28px"}}>
                            <Row className={styles.photoScroll}>
                              {photos}
                            </Row>
                          </Col>
                        </Row>
                      </Col>

                      <Col span={6}>
                        <div style={{fontWeight:'bold','marginBottom':'8px'}}>考前照片</div>
                        {StudentInfo['before_photo'] != null ?<img src={StudentInfo['before_photo'].path} style={{width:'100%','marginBottom':'8px'}}/>:"没有考前照片"}
                        <div style={{fontWeight:'bold','marginBottom':'8px'}}>考后照片</div>
                        { StudentInfo['after_photo'] != null ?<img src={StudentInfo['after_photo'].path} style={{width:'100%','marginBottom':'8px'}}/>:"没有考后照片"}

                      </Col>

                    </Row>

                  </TabPane>
                </Tabs>
                :
                <h2>没有对应考生的照片信息</h2>
            }
          </Col>
        </Row>

        <Modal title="修改分值" className={styles.editModal}   visible={this.state.scoreModalVisible} onOk={()=> this.updateScore(this.state.quizScore,this.state.quizId)} onCancel={()=>this.setState({scoreModalVisible:false})}>
         <div style={{margin:'0 auto',width:'200px'}}>
           <span style={{marginRight:'16px'}}>分值： </span> <Input placeholder={"分值："}className={styles.scoreInput} onChange={this.modifyQuizScore.bind(this)} value={this.state.quizScore}/>
         </div>
        </Modal>

        <Modal title="详细信息" className={styles.editModal} width={800}   visible={this.state.isModalVisible} onOk={()=>this.setState({isModalVisible:false})} onCancel={()=>this.setState({isModalVisible:false})}>
          <img src={this.state.mainInfo} style={{width:'100%'}}/>
        </Modal>

      </PageContainer>
      </Spin>
    );
  }
}

export default connect(({ StudentInfo, loading }) => ({
  StudentInfo,
  loading: loading.models.StudentInfo,
}))(StudentInfo);
