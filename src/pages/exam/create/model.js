import { queryFakeList, createResult,updateResult} from './service';
import { history } from 'umi';
import {examBackToFront, examFrontToBack} from "@/utils/utils"
import { message} from 'antd';

const paperInfo = {
  duration:{
    isChecked:true,
    value:0
  },
  layoutMode:{
    isChecked:true,
    value:1
  },
  isUnchangeable:{
    isChecked:true
  }
}

const Model = {
  namespace: 'examPage',
  state: {
    title:'',
    paperInfo: JSON.parse(JSON.stringify(paperInfo)) ,
    quizes: [],
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      yield put({
        type: 'quizeList',
        payload: {
          title: response.status == 'ok'?response.data.title:'',
          id: response.status == 'ok'?response.data.id:-1,
          paperInfo:response.status == 'ok'?response.data.paperInfo:JSON.parse(JSON.stringify(paperInfo)),
          quizes: response.status == 'ok'?examBackToFront(response.data.quizes):[]
        },
      });
    },
    *saveResult({payload}, { call, put }) {
      let frontToBack = examFrontToBack(payload);
      let response;
      if(frontToBack.id){
        response = yield call(updateResult, frontToBack);
      }
      else{
        response = yield call(createResult, frontToBack);
      }

      if(response.status === "ok"){
        if(!frontToBack.id){
          history.replace('/exam/edit/'+response.data.id);
        }

      }
      else{
        message.error(response.message)
      }
    },

    *submitResult({payload}, { call, put }) {
      let frontToBack = examFrontToBack(payload);
      let response;
      if(frontToBack.id){
        response = yield call(updateResult, frontToBack);
      }
      else{
        response = yield call(createResult, frontToBack);
      }

      if(response.status === "ok"){
        history.push('/exam');
        yield put({
          type: 'submitFinish',
          result: response
        })
      }
      else{
        message.error(response.message)
      }
    }
  },
  reducers: {
    modifyPaperInfo(state, action){
      return { ...state, paperInfo: action.payload.paperInfo };
    },
    modifyTitle(state, action){
      return { ...state, title: action.payload.title };
    },
    addQuiz(state, action){
      let quiz = JSON.parse(JSON.stringify(state.quizes));
      if(action.parentIndex == -1){
      quiz.splice(action.index+1, 0 ,action.value);
      }
      else{
        quiz[action.parentIndex].quizes.splice(action.index+1, 0 ,action.value);
      }

      return { ...state, quizes: quiz };
    },
    deleteQuiz(state, action){
      let quiz = JSON.parse(JSON.stringify(state.quizes));
      if(action.parentIndex == -1){
        quiz.splice(action.index, 1);
      }
      else{
        quiz[action.parentIndex].quizes.splice(action.index, 1);
      }

      return { ...state, quizes: quiz };
    },
    modifyQuiz(state, action){
      let quiz = JSON.parse(JSON.stringify(state.quizes));
      if(action.parentIndex == -1){
        quiz[action.index] = action.value;
      }
      else{
        quiz[action.parentIndex].quizes[action.index] = action.value;
      }

      return { ...state, quizes: quiz };
    },
    quizeList(state, action) {
      return  action.payload ;
    },
    submitFinish(state, action) {
      return { ...state };
    },
    reset(state, action){
        return  {
          title:'',
          paperInfo:JSON.parse(JSON.stringify(paperInfo)),
          quizes: [],
        };
    }
  },
};
export default Model;
