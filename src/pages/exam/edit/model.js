import { queryFakeList, sendResult} from './service';
import { history } from 'umi';
import {examFrontToBack ,examBackToFront} from "@/utils/utils"


const Model = {
  namespace: 'examEdit',
  state: {
    title:'',
    id:-1,
    duration:0,
    quizes: [],
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      yield put({
        type: 'quizeList',
        payload: {
          title: response.status == 'ok'?response.data.title:'',
          id: response.status == 'ok'?response.data.id:-1,
          duration:response.status == 'ok'?response.data.duration:0,
          quizes: response.status == 'ok'?examBackToFront(response.data.quizes):[]
        },
      });
    },
    *submitResult({payload}, { call, put }) {
      let frontToBack = examFrontToBack(payload);
      const response = yield call(sendResult, frontToBack);
      if(response){
        history.push('/exam');
        yield put({
          type: 'submitFinish',
          result: response
        })
      }
    }
  },
  reducers: {
    modifyDuration(state, action){
      return { ...state, duration: action.payload.duration };
    },
    modifyTitle(state, action){
      return { ...state, title: action.payload.title };
    },
    addQuiz(state, action){
      let quiz = JSON.parse(JSON.stringify(state.quizes));
      quiz.splice(action.index+1, 0 ,action.value);

      return { ...state, quizes: quiz };
    },
    deleteQuiz(state, action){
      let quiz = JSON.parse(JSON.stringify(state.quizes));
      quiz.splice(action.index, 1)
      return { ...state, quizes: quiz };
    },
    modifyQuiz(state, action){
      let quiz = JSON.parse(JSON.stringify(state.quizes));
      quiz[action.index] = action.value;
      return { ...state, quizes: quiz };
    },
    quizeList(state, action) {
      return action.payload ;
    },
    submitFinish(state, action) {
      return { ...state };
    },
    reset(state, action){
        return { ...state, title:'', quizes:[] };
    }
  },
};
export default Model;
