import request from 'umi-request';

export async function queryList(params) {
  return request(API_SERVER + 'backend-service/api/exam/info?id='+params.id
  );
}

export async function deleteExam(params) {
  return request(API_SERVER + 'backend-service/api/exam/delete?id='+params.id
   );
}
