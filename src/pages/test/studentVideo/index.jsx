import { PlusOutlined } from '@ant-design/icons';
import {Button, Card, Input, List, Tabs, Typography,Row,Col, Spin} from 'antd';
import React, { Component } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
const { Paragraph } = Typography;
import { history } from 'umi';
import moment from 'moment';
import '@opentok/client';
import { OTSession, OTPublisher, OTStreams, OTSubscriber } from 'opentok-react';

const { TabPane } = Tabs;

function callback(key) {

}


class StudentVideo extends Component {
  constructor(props) {
    super(props);
    this.state={
      bigPhoto:null
    };
  }


  componentDidMount() {
    const { dispatch } = this.props;
    const examId =this.props.match.params.examId;
    dispatch({
      type: 'StudentVideo/fetch',
      payload: {
        pageId: this.props.StudentVideo.pagination.current,
        examId:examId
      },
    });
  }

  render() {
    const {
      loading
    } = this.props;

    const studentVideo = this.props.StudentVideo.info;

    const content = (
      <div className={styles.pageHeaderContent}>
        <span className={styles.title}>{this.props.match.params.name}</span>
      </div>
    );


    return (
      <Spin spinning={loading?loading:false}>
      <PageContainer content={content} className={styles.containerWrap}>
       {
        (Object.keys(studentVideo).length > 0 ) ?
              <Tabs defaultActiveKey="1" >

              <TabPane tab="考试表现" key="1">
                <OTSession apiKey={studentVideo.api_key} sessionId={studentVideo.session_id} token={studentVideo.token} >
                  <OTStreams>
                    <OTSubscriber />
                  </OTStreams>
                </OTSession>

              </TabPane>
              </Tabs>
            :
            <h2>没有对应考生的信息</h2>
        }


      </PageContainer>
      </Spin>
    );
  }
}

export default connect(({ StudentVideo, loading }) => ({
  StudentVideo,
  loading: loading.models.StudentVideo,
}))(StudentVideo);
