import request from 'umi-request';


export async function queryUserAllPhotos(params) {
  return request( API_SERVER + `monitor-service/api/photo/users/${params.id}/images?exam_id=${params.examId}`, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}

// export async function queryUserBigPhoto(params) {
//   return request( API_SERVER + `monitor-service/api/photo/image_url?user_id=${params.id}&exam_id=${params.examId}`, {
//     // headers: { 'x-auth-token': localStorage.getItem("token") },
//   });
// }

export async function queryUserBigPhoto(params) {
  return request( API_SERVER + `backend-service/api/exam/user?userId=${params.id}&examId=${params.examId}`, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}


export async function queryUserIncidents(params) {
  return request( API_SERVER + `monitor-service/api/photo/incidents?exam_id=${params.examId}&user_id=${params.id}&event_mask=1`, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}

export async function updateCheating(params) {
  return request(API_SERVER + `backend-service/api/exam/user/cheating?cheating=${params.cheating}&examId=${params.examId}&userId=${params.id}`, {
    method:'POST',
    // headers: { 'Content-Type': 'application/json' },
    // data:JSON.stringify(params)
  });
}

export async function updateScore(params) {
  return request(API_SERVER + `backend-service/api/exam/user/score?score=${params.score}&examId=${params.examId}&userId=${params.id}&quizId=${params.quizId}`, {
    method:'POST',
    // headers: { 'Content-Type': 'application/json' },
    // data:JSON.stringify(params)
  });
}
