import { queryFakeList,sendResult } from './service';
import { history } from 'umi';

const Model = {
  namespace: 'doExam',
  state: {
    id:-1,
    title:'',
    quizes: [],
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      yield put({
        type: 'quizeList',
        payload: response.code == 200 ? response.data : {
          id:-1,
          title:'',
          quizes: [],
        },
      });
    },
    *submit({payload}, { call, put }) {
      const response = yield call(sendResult, payload);
      if(response){
        history.replace('/exam');
        yield put({
          type: 'submitResult',
          result: response
        })
      }
    }
  },
  reducers: {
    modifyQuiz(state, action){
      let quiz = JSON.parse(JSON.stringify(state.quizes));
      quiz[action.index] = action.value;
      return { ...state, quizes: quiz };
    },
    quizeList(state, action) {
      return { ...state, id: action.payload.id , title: action.payload.title ,quizes: action.payload.quizes };
    },
    submitResult(state, action) {
      return { ...state };
    },
  },
};
export default Model;
