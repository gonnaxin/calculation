const basicSetting ={

}

const examSetting ={
  '/test/create':{
    'divider1':'基础信息:',
    'area1-1':'考试名称:',
    'area1-2':'注册时间:',
    'area1-3':'考试时间:',
    'area1-4':'考试须知:',
    'area1-5':'考试试卷:',
    'area1-6':'报名方式:',
    'area1-7':'考生列表:',
    'area1-8':'科目:',
    'area1-9':'外部试卷链接:',
    'divider2':'考试流程',
    'area2-1':'主要监考方式:',
    'divider3':'监考设置',
    'area3-1':'',
  }
}

const papaerSetting = {
  0: {
    type: 0,
    description:"",
    content: [
      {
        type:1,
        text:""
      },
      //image
      {
        type:2,
        url:[],
        width:0,
        height:0
      },
      //audio
      {
        type:3,
        url:[],
        repeat:0,
        duration:0,
        autoPlay:false
      }
    ],
    isUnchangeable:true,
    layoutMode:0,
    duration:0,
    quizes:[]
  },
  1: {
    type: 1,
    description:"",
    content: [
      {
        type:1,
        text:""
      },
      //image
      {
        type:2,
        url:[],
        width:0,
        height:0
      },
      //audio
      {
        type:3,
        url:[],
        repeat:0,
        duration:0,
        autoPlay:false
      }
    ],
    isUnchangeable:true,
    score:0,
    duration:0,
    layoutMode:1,
    layoutUnit:4,
    optDisorder:true,
    options: [
      {
        "description": "",
        "isRight": false
      },
      {
        "description": "",
        "isRight": false
      },
      {
        "description": "",
        "isRight": false
      },
      {
        "description": "",
        "isRight": false
      }
    ],
  },
  6: {
    type: 6,
    content: [
      {
        type:1,
        text:""
      },
      //image
      {
        type:2,
        url:[],
        width:0,
        height:0
      },
      //audio
      {
        type:3,
        url:[],
        repeat:0,
        duration:0,
        autoPlay:false
      }
    ],
    isUnchangeable:true,
    duration:0
  },
  7: {
    type: 7,
    content: [
      {
        type:1,
        text:""
      },
      //image
      {
        type:2,
        url:[],
        width:0,
        height:0
      },
      //audio
      {
        type:3,
        url:[],
        repeat:0,
        duration:0,
        autoPlay:false
      }
    ],
    files:[//file
      {
        url: []
      }
    ],
    isUnchangeable:true,
    duration:0,
    score:0
  }
}

export {basicSetting, examSetting, papaerSetting}
