import React, {useEffect, useMemo, useRef, useState} from "react";
import {connect, history} from "umi";
import {Space, Pagination } from "antd";
import '@opentok/client';
import { OTSession, OTPublisher, OTStreams, OTSubscriber } from 'opentok-react';

const StudentVideo = (props) => {
  const {examId}= props;

  const [pagination,setPage] = useState({
    current: 1,
    pageSize: 1,
    total:5
  });
  const [info, setInfo] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchPapers(examId,pagination)
  }, []);


  const fetchPapers = (examId, pagination) => {
    setLoading(true);
    fetch(API_SERVER + `rtc-service/api/rtc/rtc/management?exam_id=${examId}&page=${pagination.current}`,
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
      .then(response => response.json())
      .then(body => {

        if(body.token != null && Object.keys(body.token).length > 0){
          setInfo(body.token);
          setPage({
            current: body.page.page,
            pageSize: 1,
            total:body.page.total_pages
          });
        }
        setLoading(false)
      });
  }

  const handleChange = (page) => {
    fetchPapers(examId, {
      current: page,
    });
  }

  return (
    (Object.keys(info).length > 0 ) ?
    <div>
      <OTSession apiKey={info.api_key} sessionId={info.session_id} token={info.token} >
        <OTStreams>
          <OTSubscriber />
        </OTStreams>
      </OTSession>
      <Pagination defaultCurrent={pagination.current} pageSize={pagination.pageSize} total={pagination.total} onChange={handleChange}/>
    </div>
    :<h2>没有对应信息</h2>
  );
};

export default StudentVideo;
