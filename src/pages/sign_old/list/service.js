import request from 'umi-request';


export async function queryFakeList(params) {
  return request( API_SERVER + `backend-service/api/sign/list?page=${params.pagination.current}&size=${params.pagination.pageSize}`, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}
