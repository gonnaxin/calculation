import request from 'umi-request';


export async function queryFakeList(params) {
  return request( API_SERVER + `backend-service/api/sign/user/list?page=${params.pagination.current}&size=${params.pagination.pageSize}&signUpId=${params.id}`, {
    // headers: { 'x-auth-token': localStorage.getItem("token") },
  });
}
