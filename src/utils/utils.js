import { parse } from 'querystring';
/* eslint no-useless-escape:0 import/prefer-default-export:0 */

const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;
export const isUrl = (path) => reg.test(path);
export const isAntDesignPro = () => {
  if (ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site') {
    return true;
  }

  return window.location.hostname === 'preview.pro.ant.design';
}; // 给官方演示站点用，用于关闭真实开发环境不需要使用的特性

export const isAntDesignProOrDev = () => {
  const { NODE_ENV } = process.env;

  if (NODE_ENV === 'development') {
    return true;
  }

  return isAntDesignPro();
};
export const getPageQuery = () => parse(window.location.href.split('?')[1]);

export const examFrontToBack = (payload) =>{
  let result = JSON.parse(JSON.stringify(payload)) ;
  for(let i=0; i<result.quizes.length;i++){
    //handle content
    let content_para = [];
    if(result.quizes[i].content[0].text != ""){
      content_para.push(result.quizes[i].content[0]);
    }
    if(result.quizes[i].content[1].url.length != 0){
      result.quizes[i].content[1].url = result.quizes[i].content[1].url[0].url;
      content_para.push(result.quizes[i].content[1]);
    }
    if(result.quizes[i].content[2].url.length != 0){
      result.quizes[i].content[2].url = result.quizes[i].content[2].url[0].url;
      content_para.push(result.quizes[i].content[2]);
    }
    result.quizes[i].content = content_para;


    //handle multiple quiz setState
    if(result.quizes[i].quizes && result.quizes[i].quizes.length >0 ){
      for(let j=0; j<result.quizes[i].quizes.length;j++){
        let inner_content_para = [];
        if(result.quizes[i].quizes[j].content[0].text != ""){
          inner_content_para.push(result.quizes[i].quizes[j].content[0]);
        }
        if(result.quizes[i].quizes[j].content[1].url.length != 0){
          result.quizes[i].quizes[j].content[1].url = result.quizes[i].quizes[j].content[1].url[0].url;
          inner_content_para.push(result.quizes[i].quizes[j].content[1]);
        }
        if(result.quizes[i].quizes[j].content[2].url.length != 0){
          result.quizes[i].quizes[j].content[2].url = result.quizes[i].quizes[j].content[2].url[0].url;
          inner_content_para.push(result.quizes[i].quizes[j].content[2]);
        }
        result.quizes[i].quizes[j].content = inner_content_para;
      }
    }

    //handle type7 files
    if(result.quizes[i].type === 7){
      result.quizes[i].files[0].url = result.quizes[i].files[0].url[0].url;
    }

  }
  return result;
}

export const examBackToFront = (quizes) =>{
  let result = [];
  for(let i=0; i<quizes.length;i++){
    let temp = JSON.parse(JSON.stringify(quizes[i])) ;

    let content_para = JSON.parse(JSON.stringify(papaerSetting[0].content));
    for(let j=0;j< quizes[i].content.length; j++ ){
      switch(quizes[i].content[j].type){
        case 1:
            content_para[0].text = quizes[i].content[j].text;
            break;
        case 2:
            content_para[1].url= [{uid:'-1',name: quizes[i].content[j].url , status:'done', url: quizes[i].content[j].url }] ;
            content_para[1].width = quizes[i].content[j].width;
            content_para[1].height = quizes[i].content[j].height;
            break;
        case 3:
            content_para[2].url = [{uid:'-1',name: '音频文件' , status:'done', url: quizes[i].content[j].url }] ;
            content_para[2].repeat = quizes[i].content[j].repeat;
            content_para[2].duration = quizes[i].content[j].duration;
            content_para[2].autoPlay = quizes[i].content[j].autoPlay;
            break;
        default: break;
      }
    }


    temp.content = content_para;


    //handle multiple quiz condition
    if(quizes[i].quizes){
     let inner_quizs_array = [];
      for(let j=0;j< quizes[i].quizes.length; j++ ){
        let inner_quiz_content = JSON.parse(JSON.stringify(quizes[i].quizes[j])) ;
        let inner_content_para =   JSON.parse(JSON.stringify(papaerSetting[0].content));

        for(let k=0;k< inner_quiz_content.content.length; k++ ){
          switch(inner_quiz_content.content[k].type){
            case 1:
              inner_content_para[0].text = inner_quiz_content.content[k].text;
              break;
            case 2:
              inner_content_para[1].url= [{uid:'-1',name: inner_quiz_content.content[k].url , status:'done', url: inner_quiz_content.content[k].url }] ;
              inner_content_para[1].width = inner_quiz_content.content[k].width;
              inner_content_para[1].height = inner_quiz_content.content[k].height;
              break;
            case 3:
              inner_content_para[2].url = [{uid:'-1',name: inner_quiz_content.content[k].url , status:'done', url: inner_quiz_content.content[k].url }] ;
              inner_content_para[2].repeat = inner_quiz_content.content[k].repeat;
              inner_content_para[2].duration = inner_quiz_content.content[k].duration;
              inner_content_para[2].autoPlay = inner_quiz_content.content[k].autoPlay;
              break;
            default: break;
          }
        }

        inner_quiz_content.content = inner_content_para;

        inner_quizs_array.push(inner_quiz_content);
      }
      temp.quizes = inner_quizs_array;

    }

    //handle type7 files
    if(quizes[i].type === 7){
      temp.files[0].url = [{uid:'-1',name: '音频文件' , status:'done', url: quizes[i].files[0].url }] ;
    }

    result.push(temp);
  }
  return result;
}
