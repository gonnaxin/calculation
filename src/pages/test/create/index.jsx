import { PlusOutlined } from '@ant-design/icons';
import React, { Component  } from 'react';
import { Row, Col, Divider } from 'antd';
import { Tabs , Input, Card ,Button , Modal,Select,Spin, Popover,DatePicker,Upload,message, Checkbox,Steps } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
import moment from 'moment';
import 'moment/locale/zh-cn';
import locale from 'antd/lib/locale/zh_CN';
import { UploadOutlined } from '@ant-design/icons';
import debounce from 'lodash/debounce';


const { RangePicker } = DatePicker;
const { Step } = Steps;

class TestCreate extends Component {
  constructor() {
    super();
    this.state= {
      paperList:[],
      fetching: false,
      step:0
    }
    this.submit = this.submit.bind(this);
    this.fetchPapers = debounce(this.fetchPapers, 800);
    this.lastFetchId = 0;
  }
  componentDidMount() {
    const { dispatch } = this.props;

    const id =this.props.match.params.id;


    if(id){
      dispatch({
        type: 'testCreate/fetchAllData',
        payload:id
      });
    }
    else{
      dispatch({
        type: 'testCreate/reset',
      });
      dispatch({
        type: 'testCreate/fetch',
      });
    }


   this.fetchPapers('')
  }

  submit(){
    const result = this.validate(this.props.testCreate);
    if(!result){
      return;
    }

    const { dispatch } = this.props;
    dispatch({
      type: 'testCreate/submitResult',
      payload:this.props.testCreate
    });

  }

  validate(info){

    if(info.title && info.title.split() === '' ){
      message.error(examSetting['/test/create']['area1-1']+"为必填项，请修改");
      return false;
    }

    if(info.title && info.title.length > 50 ){
      message.error(examSetting['/test/create']['area1-1']+"不能超过50个字符");
      return false;
    }
    if(info.subject && info.subject.length > 50 ){
      message.error(examSetting['/test/create']['area1-8']+"不能超过50个字符");
      return false;
    }
    if(info.description && info.description.length > 1024 ){
      message.error(examSetting['/test/create']['area1-4']+"不能超过1024个字符");
      return false;
    }
    if(info.paperLink && info.paperLink.length > 1024 ){
      message.error(examSetting['/test/create']['area1-9']+"不能超过1024个字符");
      return false;
    }
    if(!info.startTime || !info.endTime){
      message.error(examSetting['/test/create']['area1-3']+"为必填项，请修改");
      return false;
    }
    if(info.paper.id === -1  && !info.paperLink){
      message.error(examSetting['/test/create']['area1-5']+"或者"+examSetting['/test/create']['area1-5'] + '至少有一个存在');
      return false;
    }


    return true;
  }


  fetchPapers(params){
    this.lastFetchId += 1;
    const fetchId = this.lastFetchId;
    this.setState({ data: [], fetching: true });
    fetch(API_SERVER + 'backend-service/api/paper/list?page=1&size=100&keyword='+params,
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'x-auth-token': localStorage.getItem("token"),
        },
        // redirect: 'follow', // manual, *follow, error
        // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
      .then(response => response.json())
      .then(body => {
        if (fetchId !== this.lastFetchId) {
          // for fetch callback order
          return;
        }

        if(body.status =='ok'){
          this.setState({paperList:body.data.list});
        }
        this.setState({ fetching: false });
      });
  }

  changeState(key,e){
    this.props.dispatch({
      type: 'testCreate/modifyState',
      payload: {
        key:key,
        value: e.target.value,
      },
    });
  }

  changeStateValue(key,value){
    this.props.dispatch({
      type: 'testCreate/modifyState',
      payload: {
        key:key,
        value: value,
      },
    });
  }

  changePaperValue(value){
    let changedValue ={id:value.value, title:value.label};
    this.props.dispatch({
      type: 'testCreate/modifyState',
      payload: {
        key:'paper',
        value: changedValue,
      },
    });
  }

  onRegisterOk(key, value) {
    value=[value[0]?moment(value[0]).valueOf():'',value[1]?moment(value[1]).valueOf():''];
    //input bigger time in first time picker, plugin will change visual value
    if((value[0] !== '' && value[1] !== '') && (value[0] > value[1])){
      value= [value[1], value[0]]
    }
    this.props.dispatch({
      type: 'testCreate/modifyTimeState',
      payload: {
        key:key,
        value: value
      },
    });
  }

  updateProcess(index, e) {
   let temp = JSON.parse(JSON.stringify( this.props.testCreate.processCfg));
    // photo before exam union photo after exam
    if(index === 2 || index === 4){
      temp.tasks[2].checked = e.target.checked;
      temp.tasks[4].checked = e.target.checked;
    }else{
      temp.tasks[index].checked = e.target.checked;
    }

     this.props.dispatch({
       type: 'testCreate/modifyState',
       payload: {
         key:'processCfg',
         value: temp,
       },
     });
  }


  changePhotoIntervalValue(value){
      let temp = JSON.parse(JSON.stringify( this.props.testCreate.monitorConfig));
      temp.photoInterval = value;
      this.props.dispatch({
        type: 'testCreate/modifyState',
        payload: {
          key:'monitorConfig',
          value: temp,
        },
      });
  }

  updatePhotoConfig(index1,index2, key, e){


    let temp = JSON.parse(JSON.stringify( this.props.testCreate.monitorConfig));

    if(index2 === -1){
      if(key === 'isChecked'){
        temp.monitorCategories[index1][key] = e.target.checked;
        //control inner level checkbox
        temp.monitorCategories[index1].events.map(function(item){
          return item.isChecked =  e.target.checked
        })
      }
    }
    else{
      if(key === 'isChecked'){
        //control outer level checkbox
        if(e.target.checked){
            temp.monitorCategories[index1].isChecked = true;
        }
        temp.monitorCategories[index1].events[index2][key] = e.target.checked;
      }
      else{
        temp.monitorCategories[index1].events[index2][key] = e.target.value;
      }
    }

    this.props.dispatch({
      type: 'testCreate/modifyState',
      payload: {
        key:'monitorConfig',
        value: temp,
      },
    });
  }

  render() {

    const {
      loading,
    } = this.props;


    const { TextArea } = Input;
    const { Option } = Select;
    const self =this;

    const props = {
      action: API_SERVER + 'upload-service/api/upload/file',
      headers: {
        'x-auth-token': localStorage.getItem("token"),
      },
      onChange(info) {
        let fileList = [...info.fileList];

        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-1);

        // 2. Read from response and show file link
        fileList = fileList.map(file => {
          if (file.response && file.response.status == 'ok') {
            // Component will show file.url as link
            file.url = file.response.data.url;
          }
          return file;
        });

        self.changeStateValue('examineeCsvUrl',fileList)
      },
    };



    const content = (
      <div className={styles.pageHeaderContent}>
        <span className={styles.title}>{this.props.match.params.id?"编辑考试":'创建考试'}</span>
           <Button type="primary" className={styles.buttonGroups} onClick={this.submit}>保存</Button>
      </div>
    );

    const next = () => {
      this.setState({step: this.state.step + 1});
    };

    const prev = () => {
      this.setState({step: this.state.step - 1});
    };


    const steps = [
      {
        title: examSetting['/test/create']['divider1'],
        content: [
          <Row className={styles.testContainer}  key={1}>
            <Col span={24} className={styles.formArea}>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-1']}</Col>
                <Col span={16} ><Input  className={styles.editGroup} onChange={this.changeState.bind(this,'title')} value={this.props.testCreate.title}/></Col>
              </Row>
              <Row gutter={[8,16]}>  <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-8']}</Col>
                <Col span={16} ><Input  className={styles.editGroup} onChange={this.changeState.bind(this,'subject')} value={this.props.testCreate.subject}/></Col>
              </Row>

              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-3']}</Col>
                <Col span={16} ><RangePicker
                  showTime={{ format: 'HH:mm:ss' }}
                  format="YYYY-MM-DD HH:mm:ss"
                  style={{width:'100%'}}
                  onOk={this.onRegisterOk.bind(this,'test')}
                  value={[this.props.testCreate.startTime == ''?null: moment(this.props.testCreate.startTime),
                    this.props.testCreate.endTime == ''?null: moment(this.props.testCreate.endTime)]}
                /></Col>
              </Row>
              <Row gutter={[8,16]}>  <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-4']}</Col>
                <Col span={16} ><Input  className={styles.editGroup} onChange={this.changeState.bind(this,'description')} value={this.props.testCreate.description}/></Col>
              </Row>
            </Col>
            <Col span={24} className={styles.formArea}>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-5']}</Col>
                <Col span={16} ><Select  style={{ width: '100%',textAlign:"left"}} showSearch={true}
                                         notFoundContent={this.state.fetching ? <Spin size="small" /> : <div>Not Found</div>}
                                         filterOption={false}
                                         onSearch={this.fetchPapers.bind(this)}
                                         onChange={this.changePaperValue.bind(this)}
                                         value={{value:this.props.testCreate.paper.id===-1?"":this.props.testCreate.paper.id}}
                                         labelInValue
                >
                  {this.state.paperList.map(d => (
                    <Option key={d.id} value={d.id}>{d.title}</Option>
                  ))}
                </Select></Col>
              </Row>
              <Row gutter={[8,16]}>  <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-9']}</Col>
                <Col span={16} ><Input  className={styles.editGroup} onChange={this.changeState.bind(this,'paperLink')} value={this.props.testCreate.paperLink}/></Col>
              </Row>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-6']}</Col>
                <Select  style={{ width: 120,textAlign:"left",margin:'0px 4px'}} onChange={this.changeStateValue.bind(this,'signUpType')} value={this.props.testCreate.signUpType}>
                  <Select.Option value={0}>线下报名</Select.Option>
                </Select>
              </Row>
              <Row gutter={[8,16]}> <Col span={4} className={styles.formTitle}>{examSetting['/test/create']['area1-7']}</Col>
                <Col span={16} style={{ textAlign:"left"}}>
                  <Upload   {...props} fileList={this.props.testCreate.examineeCsvUrl}>
                    <Button icon={<UploadOutlined />}>Click to Upload</Button>
                  </Upload>
                </Col>
              </Row>
            </Col>
          </Row>
        ],
      },
      {
        title: examSetting['/test/create']['divider2'],
        content: [
          <Row className={styles.testContainer}  key={2} >
            <Col span={24} className={styles.formArea} style={{'textAlign':'left','marginLeft':'0px'}}>
              {
                this.props.testCreate.processCfg.tasks && this.props.testCreate.processCfg.tasks.map((item,index) => {
                  const beforeInfo = <div className={styles.configAreaBorder}>
                    <Row style={{margin:"18px 24px 32px 24px"}}> <Col span={24} className={styles.formTitle}>{examSetting['/test/create']['area1-2']}</Col>
                      <Col span={24} style={{marginTop:"8px"}}><RangePicker
                        showTime={{ format: 'HH:mm:ss' }}
                        format="YYYY-MM-DD HH:mm:ss"
                        style={{width:'100%'}}
                        onOk={this.onRegisterOk.bind(this,'register')}
                        value={[this.props.testCreate.registerStartTime === ''?null: moment(this.props.testCreate.registerStartTime),
                          this.props.testCreate.registerEndTime === ''?null: moment(this.props.testCreate.registerEndTime)]}
                      /></Col>
                    </Row>
                  </div>;
                  const testInfo =<div className={styles.configAreaBorder}>
                    <Row style={{margin:"18px 24px 32px 24px"}}> <Col  className={styles.formTitle}>{examSetting['/test/create']['area1-3']}</Col>
                      <Col span={20} style={{margin:"4px 0px 32px 16px"}}>
                        {
                          (this.props.testCreate.startTime === ''?"": moment(this.props.testCreate.startTime).format("YYYY-MM-DD HH:mm:ss")) + ' - ' +
                          (this.props.testCreate.endTime === ''?"":moment(this.props.testCreate.endTime).format("YYYY-MM-DD HH:mm:ss"))
                        }
                      </Col>
                      <Col span={24}>
                        考试结束前  <Input style={{width:"64px"}} onChange={this.changeState.bind(this,'remindBeforeEnd')} value={this.props.testCreate.remindBeforeEnd} />
                        秒，提醒考生
                      </Col>
                    </Row>
                  </div>;

                  if(item.taskName === '考前注册'){
                    return item.mandatory?
                      <div className={styles.testProcessPara} key={index}><Checkbox  checked={item.checked} disabled={true}> {item.taskName}</Checkbox>{beforeInfo}</div>
                      :
                      <div  className={styles.testProcessPara} key={index}><Checkbox  onChange={this.updateProcess.bind(this,index)} checked={item.checked}> {item.taskName}</Checkbox>{beforeInfo}</div>
                  }
                  if (item.taskName === '考试'){
                    return item.mandatory?
                      <div className={styles.testProcessPara} key={index}><Checkbox  checked={item.checked} disabled={true}> {item.taskName}</Checkbox>{testInfo}</div>
                      :
                      <div  className={styles.testProcessPara} key={index}><Checkbox  onChange={this.updateProcess.bind(this,index)} checked={item.checked}> {item.taskName}</Checkbox>{testInfo}</div>
                  }
                  return item.mandatory?
                    <div className={styles.testProcessPara}  key={index}><Checkbox checked={item.checked} disabled={true}> {item.taskName}</Checkbox></div>
                    :
                    <div  className={styles.testProcessPara} key={index}><Checkbox  onChange={this.updateProcess.bind(this,index)} checked={item.checked}> {item.taskName}</Checkbox></div>
                })
              }
            </Col>
          </Row>
        ],
      },
      {
        title: examSetting['/test/create']['divider3'],
        content: [
          <Row className={styles.testContainer}   key={3}>
            <Col span={24} className={""}>
              <Row span={24}>
                <Col span={24} style={{marginBottom:"8px"}}>{examSetting['/test/create']['area2-1']}</Col>
                <Col style={{marginRight:"24px"}}>
                  <Select style={{width:"160px"}}  onChange={this.changeStateValue.bind(this,'invigilateType')} value={this.props.testCreate.invigilateType}>
                    <Select.Option value={0}>不监考</Select.Option>
                    <Select.Option value={1}>拍照</Select.Option>
                    <Select.Option value={2}>直播</Select.Option>
                  </Select>
                </Col>
                {this.props.testCreate.invigilateType === 1?<Col span={8}>
                  拍照间隔：<Select style={{width:"160px"}}  onChange={this.changePhotoIntervalValue.bind(this)} value={this.props.testCreate.monitorConfig.photoInterval}>
                    <Select.Option value={15}>15s</Select.Option>
                    <Select.Option value={30}>30s</Select.Option>
                    <Select.Option value={45}>45s</Select.Option>
                  </Select>
                </Col>:""}

              </Row>
            </Col>
            <Col span={24} className={""}>
            {
              this.props.testCreate.invigilateType === 1?


                <div className={styles.configArea}>
                  <Row>
                    <Checkbox  onChange={this.updatePhotoConfig.bind(this,0, -1, 'isChecked')} checked={this.props.testCreate.monitorConfig.monitorCategories[0].isChecked}> {"图像识别"}</Checkbox>
                  </Row>
                  <Row className={styles.configAreaBorder}>
                    <Col  span={24} className={styles.configAreaItem}>
                      <Checkbox onChange={this.updatePhotoConfig.bind(this,0, 0, 'isChecked')}  checked={this.props.testCreate.monitorConfig.monitorCategories[0].events[0].isChecked}> {"检测到无法识别面部记录。"}</Checkbox>
                    </Col>
                    <Col  span={24} className={styles.configAreaItem}>
                      <Checkbox onChange={this.updatePhotoConfig.bind(this,0, 1, 'isChecked')}   checked={this.props.testCreate.monitorConfig.monitorCategories[0].events[1].isChecked}> {"检测到识别到多个面部记录。"}</Checkbox>
                    </Col>
                    <Col  span={24} className={styles.configAreaItem}>
                      <Checkbox onChange={this.updatePhotoConfig.bind(this,0, 2, 'isChecked')}   checked={this.props.testCreate.monitorConfig.monitorCategories[0].events[2].isChecked}> {"检测到出现非本人面部记录。"}</Checkbox>
                    </Col>
                    <Col  span={24} className={styles.configAreaItem}>
                      <Checkbox  onChange={this.updatePhotoConfig.bind(this,0, 3, 'isChecked')}  checked={this.props.testCreate.monitorConfig.monitorCategories[0].events[3].isChecked}> {"检测到使用手机记录。"}</Checkbox>
                    </Col>
                  </Row>
                </div>
                : ""
             }
              {
                this.props.testCreate.monitorConfig && this.props.testCreate.monitorConfig.monitorCategories ? <div className={styles.configArea}>
                  <Row>
                    <Checkbox  onChange={this.updatePhotoConfig.bind(this,1, -1, 'isChecked')} checked={this.props.testCreate.monitorConfig.monitorCategories[1].isChecked}> {"智能检测"}</Checkbox>
                  </Row>
                  <Row className={styles.configAreaBorder}>
                    <Col  span={24} className={styles.configAreaItem}>
                      <Checkbox onChange={this.updatePhotoConfig.bind(this,1, 2, 'isChecked')}  checked={this.props.testCreate.monitorConfig.monitorCategories[1].events[2].isChecked}> {"霸屏考试"}</Checkbox>
                    </Col>
                    <Col  span={24} className={styles.configAreaItem}>
                      <Checkbox onChange={this.updatePhotoConfig.bind(this,1, 3, 'isChecked')}   checked={this.props.testCreate.monitorConfig.monitorCategories[1].events[3].isChecked}> {"禁止特殊操作"}</Checkbox>
                    </Col>
                    <Col  span={24} className={styles.configAreaItem}>
                      <Checkbox onChange={this.updatePhotoConfig.bind(this,1, 0, 'isChecked')}   checked={this.props.testCreate.monitorConfig.monitorCategories[1].events[0].isChecked}>
                        考生切屏出登录
                      </Checkbox>
                    </Col>
                    <Col  span={24} className={styles.configAreaItem}>
                      <Checkbox  onChange={this.updatePhotoConfig.bind(this,1, 1, 'isChecked')}  checked={this.props.testCreate.monitorConfig.monitorCategories[1].events[1].isChecked}>
                        {"本次考试内考生登录次数达到"}  <Input  className={styles.innerInput} onChange={this.updatePhotoConfig.bind(this,1,1, 'limit')} value={this.props.testCreate.monitorConfig.monitorCategories[1].events[1].limit}/>
                        {"次时，强制结束考试"}
                      </Checkbox>
                    </Col>
                  </Row>
                </div>:""
              }

            </Col>

          </Row>
        ],
      },
    ];

    return (
      <Spin spinning={loading?loading:false}>
        <PageContainer content={content} >
          <div style={{overflow:'hidden'}}>
            <Steps direction="vertical" size="small" current={this.state.step} className={styles.steps}>
              {steps.map(item => (
                <Step key={item.title} title={item.title} />
              ))}
            </Steps>
            <div className={styles.stepsContent}>{steps[this.state.step].content}</div>
          </div>
          <div className={styles.stepsAction}>
            {this.state.step > 0 && (
              <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
                上一步
              </Button>
            )}
            {this.state.step < steps.length - 1 && (
              <Button type="primary" onClick={() => next()}>
                下一步
              </Button>
            )}
          </div>

        </PageContainer>
      </Spin>
    );
  }
}

export default connect(({ testCreate, loading }) => ({
  testCreate,
  loading: loading.models.testCreate,
}))(TestCreate);
