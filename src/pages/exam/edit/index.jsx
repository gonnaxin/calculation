import { PlusOutlined } from '@ant-design/icons';
import React, { Component  } from 'react';
import { Row, Col, Divider } from 'antd';
import { Tabs , Input, Card ,Button , Modal,Select,Spin, Popover,Radio, message } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';

const { TabPane } = Tabs;

function callback(key) {

}


class ExamEdit extends Component {
  constructor() {
    super();
    this.state= {
      isModalVisible: false,
      isAddModalVisible:false,
      currentIndex: 0,
      currentSubject: {}
    }
  }
  componentDidMount() {
    const { dispatch } = this.props;
    const params =this.props.match.params.id;
    dispatch({
      type: 'examEdit/fetch',
      payload:params
    });
  }

  submit(){
    const { dispatch } = this.props;
    dispatch({
      type: 'examEdit/submitResult',
      payload:this.props.examEdit
    });
  }

  titleChange(e){
    this.props.dispatch({
      type: 'examEdit/modifyTitle',
      payload: {
        title: e.target.value,
      },
    });
  }

  durationChange(e){
    this.props.dispatch({
      type: 'examEdit/modifyDuration',
      payload: {
        duration: Number(e.target.value),
      },
    });
  }

  addQuiz(index,value){

    // this.props.dispatch({
    //   type: 'examEdit/addQuiz',
    //   questionType: Number(value),
    //   index:index
    // });

    if(Number(value) === 1){
      this.setState({
        currentSubject: JSON.parse(JSON.stringify(papaerSetting[1])),
        currentIndex: index
      },function(){
        this.setState({
          isAddModalVisible:true
        })
      })
    }

  }
  deleteQuiz(index){
    this.props.dispatch({
      type: 'examEdit/deleteQuiz',
      index:index
    });
  }

  modifyQuizGeneral(key, e){
    let currentSubject = this.state.currentSubject;
    currentSubject[key] = e.target.value
    this.setState({
      currentSubject:currentSubject
    })
  }

  modifyQuizTitle(e){
    let currentSubject = this.state.currentSubject;
    currentSubject.description[0].content = e.target.value
    this.setState({
      currentSubject:currentSubject
    })
  }

  modifyQuizOptions(optionsIndex,e){
    let currentSubject = this.state.currentSubject;
    currentSubject.options[optionsIndex].description = e.target.value
    this.setState({
      currentSubject:currentSubject
    })
  }

  getAnswerIndex(options){
    for(let i=0;i<options.length;i++){
      if(options[i].isRight){
        return i;
      }
    }
    return -1;
  }

  modifyQuizAnswer(value){
    let currentSubject = this.state.currentSubject;
    for(let i=0;i<currentSubject.options.length;i++){
      currentSubject.options[i].isRight = false;
    }
    currentSubject.options[value].isRight = true;
    this.setState({
      currentSubject:currentSubject
    })
  }

  showModal(index) {
    this.setState({
      currentSubject: JSON.parse(JSON.stringify(this.props.examEdit.quizes[index])),
      currentIndex: index
    },function(){
      this.setState({
        isModalVisible:true
      })
    })

  }

  handleOk() {
    let result = this.validate(this.state.currentSubject);
    if(result){
      this.props.dispatch({
        type: 'examEdit/modifyQuiz',
        index:this.state.currentIndex,
        value:this.state.currentSubject
      });
      this.setState({
        isModalVisible:false
      })
    }
  }

  handleCancel(){
    this.setState({isModalVisible:false})
  }

  handleAddOk() {
    let result = this.validate(this.state.currentSubject);
    if(result){
      this.props.dispatch({
        type: 'examEdit/addQuiz',
        index:this.state.currentIndex,
        value:this.state.currentSubject
      });

      this.setState({
        isAddModalVisible:false
      })
    }
  }

  handleAddCancel(){
    this.setState({isAddModalVisible:false})
  }


  questionType (index){
    return (
      <div>
        <Select  style={{ width: 120 }}   onSelect={this.addQuiz.bind(this,index)} >
          <Select.Option value="1">单选题</Select.Option>
          <Select.Option value="2">多选题</Select.Option>
        </Select>
      </div>
    )
  }

  validate(current){
    // if(current.description == ""){
    //     message.error("题目的题干不能为空");
    //     return false;
    // }
    // for (let index in current.options){
    //   if(current.options[index].description == "" ){
    //     message.error("题目的选项不能为空");
    //     return false;
    //   }
    // }
    return true;
  }

  render() {

    const {
      examEdit: {},
      loading,
    } = this.props;


    const { TextArea } = Input;
    const { Option } = Select;

    const content = (
      <div className={styles.pageHeaderContent}>
        <span className={styles.title}>{'编辑试题'}</span>
           <Button type="primary" className={styles.buttonGroups} onClick={this.submit.bind(this)}>保存</Button>
      </div>
    );




    let modalInfo = (<div></div>);

    let res = [];
      for(let i = 0; i < this.props.examEdit.quizes.length; i++) {
        let answer =this.getAnswerIndex(this.props.examEdit.quizes[i].options);
        switch (this.props.examEdit.quizes[i].type) {
          case 1: res.push(
            <Card title={i+1+". "+this.props.examEdit.quizes[i].description[0].content} bordered={false} className={styles.quizCard} key={i}>
            <Radio.Group value={answer} className={styles.radioGroup}>
              <Radio value={0}>{this.props.examEdit.quizes[i].options[0].description}</Radio>
              <Radio value={1}>{this.props.examEdit.quizes[i].options[1].description}</Radio>
              <Radio value={2}>{this.props.examEdit.quizes[i].options[2].description}</Radio>
              <Radio value={3}>{this.props.examEdit.quizes[i].options[3].description}</Radio>
            </Radio.Group>
              <a type="primary" className={styles.buttonNoBackground} data-index ={i} onClick={this.deleteQuiz.bind(this,i)}>删除</a>
              <Popover content={this.questionType.bind(this,i)} title="选择题型" trigger="hover" key={i}>
              <a type="primary" className={styles.buttonNoBackground}data-index ={i}  >添加</a>
              </Popover>
              <a type="primary" className={styles.buttonNoBackground} data-index ={i} onClick={this.showModal.bind(this,i)} >编辑</a>
            </Card>
          )
            break;
          default: break;
        }
      }

      if(res.length == 0){
        res =(<Popover content={this.questionType.bind(this,-1)} title="选择题型" trigger="hover">
               <Button type="" className={styles.buttonAddFirst} data-index ={-1}  >添加试题</Button>
             </Popover>)
       }

      switch (this.state.currentSubject.type) {
        case 1: modalInfo = (
          <div>
            <p>题目：</p> <TextArea rows={4} placeholder="请输入题目" className={styles.editGroup} onChange={this.modifyQuizTitle.bind(this)} value={this.state.currentSubject.description[0].content}/>
            <p>选项1</p> <Input placeholder="请输入选项1" className={styles.editGroup} onChange={this.modifyQuizOptions.bind(this,'0')} value={this.state.currentSubject.options[0].description}/>
            <p>选项2</p> <Input placeholder="请输入选项2" className={styles.editGroup} onChange={this.modifyQuizOptions.bind(this,'1')} value={this.state.currentSubject.options[1].description}/>
            <p>选项3</p> <Input placeholder="请输入选项3" className={styles.editGroup} onChange={this.modifyQuizOptions.bind(this,'2')} value={this.state.currentSubject.options[2].description}/>
            <p>选项4</p> <Input placeholder="请输入选项4" className={styles.editGroup} onChange={this.modifyQuizOptions.bind(this,'3')} value={this.state.currentSubject.options[3].description}/>
            <p style={{ width: 80,display:'inline' }}>正确答案：</p>
            <Select  style={{ width: 100 }} onChange={this.modifyQuizAnswer.bind(this)} value={''+this.getAnswerIndex(this.state.currentSubject.options)}>
              <Select.Option value="0">第一个</Select.Option>
              <Select.Option value="1">第二个</Select.Option>
              <Select.Option value="2" >第三个</Select.Option>
              <Select.Option value="3">第四个</Select.Option>
            </Select>
            <p style={{ width: 80,display:'inline',marginLeft:'24px'  }}>答题时间：</p>
            <Input  style={{ width: 80}} placeholder="" className={styles.editGroup} onChange={this.modifyQuizGeneral.bind(this,'duration')} value={this.state.currentSubject.duration}/>
            <p style={{ width: 80,display:'inline',marginLeft:'24px'  }}>分值：</p>
            <Input  style={{ width: 80 }} placeholder="" className={styles.editGroup} onChange={this.modifyQuizGeneral.bind(this,'score')} value={this.state.currentSubject.score}/>
          </div>
        )
          break;

        default:modalInfo = (<div></div>)
      }


    return (
      <Spin spinning={loading?loading:false}>
        <PageContainer content={content} >
          <Row className={styles.quizContainer}  >
            <Col className={styles.quizMain} span={6} >
              <Tabs defaultActiveKey="1" onChange={callback} centered>
                <TabPane tab="试卷信息" key="1">
                  <Row span={24} className={styles.formRow}>
                    <Col span={6} >试卷名称:</Col>
                  </Row>
                  <Row span={24} className={styles.formRow}>
                    <Col span={24} ><Input placeholder="试卷名称" onChange={this.titleChange.bind(this)} value={this.props.examEdit.title}/></Col>
                  </Row>
                  <Row span={24} className={styles.formRow}>
                    <Col span={6} style={{marginTop:'4px'}}>答题时间:</Col>
                    <Col span={6} ><Input placeholder="" onChange={this.durationChange.bind(this)} value={this.props.examEdit.duration}/></Col>
                    <Col span={6} style={{marginTop:'4px'}}>秒</Col>
                  </Row>
                </TabPane>
                <TabPane tab="题目大纲" key="2" disabled>

                </TabPane>
              </Tabs>
            </Col>
            <Col className={styles.quizInfo} span={17} offset={1} >
              {res}
            </Col>
          </Row>
          <Modal  title="编辑题目" className={styles.editModal} width={800} okText={"保存"} cancelText={"取消"} visible={this.state.isModalVisible} onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)} closable={false} maskClosable={false} >
            {modalInfo}
          </Modal>
          <Modal title="添加题目" className={styles.editModal} width={800}  okText={"保存"} cancelText={"取消"} visible={this.state.isAddModalVisible} onOk={this.handleAddOk.bind(this)} onCancel={this.handleAddCancel.bind(this)} closable={false} maskClosable={false} >
            {modalInfo}
          </Modal>
        </PageContainer>
      </Spin>
    );
  }
}

export default connect(({ examEdit, loading }) => ({
  examEdit,
  loading: loading.models.examEdit,
}))(ExamEdit);
