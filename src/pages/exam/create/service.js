import request from 'umi-request';

export async function queryFakeList(params) {
  return request(API_SERVER + 'backend-service/api/paper/info?id='+params);
}

export async function updateResult(params) {
  return request(API_SERVER + 'backend-service/api/paper/update', {
    method:'POST',
    headers: { 'Content-Type': 'application/json' },
    data:JSON.stringify(params)
  });
}

export async function createResult(params) {
  return request(API_SERVER + 'backend-service/api/paper/create', {
    method:'POST',
    headers: { 'Content-Type': 'application/json' },
    data:JSON.stringify(params)
  });
}
