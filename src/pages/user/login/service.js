import request from 'umi-request';

export async function fakeAccountLogin(params) {
  return request('/api/login/account', {
    method: 'POST',
    data: params,
  });
}

export async function accountLogin(params) {
  return request(API_SERVER +'auth-service/api/login', {
    method: 'POST',
    // headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
    data:{'username':params.userName+params.orgName,'password':params.password},
    requestType: 'form',
    responseType: 'json'
  });
}



export async function getFakeCaptcha(mobile) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}
