import request from 'umi-request';

export async function processList(params) {
  return request(API_SERVER +'backend-service/api/exam/process/config');
}

export async function photoList(params) {
  return request(API_SERVER +'monitor-service/api/photo/config');
}

export async function sendResult(params) {
  return request(API_SERVER + 'backend-service/api/exam/create', {
    method:'POST',
    headers: { 'Content-Type': 'application/json' },
    data:JSON.stringify(params)
  });
}

export async function fetchAllData(params) {
  return request(API_SERVER + 'backend-service/api/exam/info?id='+params
  );
}

export async function sendUpdateResult(params) {
  return request(API_SERVER + 'backend-service/api/exam/update', {
    method:'POST',
    headers: { 'Content-Type': 'application/json' },
    data:JSON.stringify(params)
  });
}
