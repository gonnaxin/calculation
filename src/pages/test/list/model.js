import { queryFakeList } from './service';

const Model = {
  namespace: 'testList',
  state: {
    list: [],
    doingList:[],
    finishList:[],
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      if(response){
        yield put({
          type: 'queryList',
          payload: response.code == 200 ? response.data.list : [],
        });
      }
    },
    *fetchDoing({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      if(response){
        yield put({
          type: 'doingList',
          payload: response.code == 200 ? response.data.list : [],
        });
      }
    },
    *fetchFinish({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      if(response){
        yield put({
          type: 'finishList',
          payload: response.code == 200 ? response.data.list : [],
        });
      }
    },

  },
  reducers: {
    queryList(state, action) {
      return { ...state, list: action.payload };
    },
    doingList(state, action) {
      return { ...state, doingList: action.payload };
    },
    finishList(state, action) {
      return { ...state, finishList: action.payload };
    },
  },
};
export default Model;
