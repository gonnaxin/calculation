import { PlusOutlined } from '@ant-design/icons';
import {Button, Card, Input, List, Space, Table, Tabs, Typography,} from 'antd';
import React, { Component } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { connect } from 'umi';
import styles from './style.less';
const { Paragraph } = Typography;
import { history } from 'umi';
import moment from 'moment';

const { TabPane } = Tabs;

function callback(key) {

}


class SignList extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'signList/fetch',
      payload: {
        pagination:{
          current: this.props.pagination.current,
          pageSize: this.props.pagination.pageSize,
          total: this.props.pagination.total,
        }
      },
    });
  }

  jump(id){
    history.push(`/sign/studentList/${id}`);
  }

  handleTableChange = (pagination, filters, sorter,e) => {
    // console.log(pagination,filters, sorter)
    // this.fetch({
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   pagination,
    //   ...filters,
    // });

    //much action may activate table change,only handle paginate part
    if(e.action != 'paginate'){
      return;
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'signList/fetch',
      payload: {
        pagination:{
          current: pagination.current,
          pageSize:  pagination.pageSize,
          total: pagination.total,
        }
      },
    });
  };


  render() {


    const {
      list,
      loading,
      pagination
    } = this.props;

    const columns = [
      {
        key:1,
        title: '名称',
        dataIndex: 'name',
        // sorter: true,
        // sortOrder:'descend',
        width: '20%'
      },
      {
        key:2,
        title: '报名开放时间',
        dataIndex: 'duration',
        width: '15%',
        render: (text, record) => {
          return (
            <div>{moment(record.startTime).format('YYYY/MM/DD')+' - '+moment(record.endTime).format('YYYY/MM/DD')}</div>
          )
        }
      },
      {
        key:3,
        title: '考试信息',
        dataIndex: 'description',
        width: '15%',
      },
      {
        key:4,
        title: '报名人数',
        dataIndex: 'enrolledUser',
        width: '15%',
      },
      {
        key:5,
        title: '报名人次',
        dataIndex: 'enrolledNumber',
        width: '15%',
      },

      {
        key:5,
        title: '操作',
        dataIndex: 'operations',
        width: '20%',
        render: (text, record) => {
          return (
            <Space size="middle">
              <a onClick={() =>(this.jump(record.id))}>查看</a>
            </Space>
          )
        }
      }
    ];

    const content = (
      <div className={styles.pageHeaderContent}>

      </div>
    );

    const nullData = {};
    return (
      <PageContainer content={content} className={styles.containerWrap}>
        <Table
          columns={columns}
          rowKey={record => record.id}
          dataSource={list}
          pagination={pagination}
          loading={loading}
          onChange={this.handleTableChange}
        />
      </PageContainer>
    );
  }
}

export default connect(({ signList, loading }) => ({
  list: signList.list,
  pagination: signList.pagination,
  loading: loading.effects['signList/fetch'],
}))(SignList);
